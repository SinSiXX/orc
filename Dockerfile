FROM debian:buster
LABEL maintainer "emery@deadcanaries.org"
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get -yq upgrade && DEBIAN_FRONTEND=noninteractive apt-get -yq install wget apt-transport-https gnupg curl libssl-dev git python build-essential tor libsecret-1-dev
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash -
RUN DEBIAN_FRONTEND=noninteractive apt-get -yq install nodejs
ENV GRANAX_USE_SYSTEM_TOR="1"
RUN git clone https://gitlab.com/deadcanaries/orc /root/orc && \
    cd /root/orc && \
    git fetch --tags && \
    git checkout $(git describe --tags `git rev-list --tags --max-count=1`) && \
    npm install --unsafe-perm --production
ENV orcd_BridgeHostname="0.0.0.0"
VOLUME ["/root/.config/orc"]
EXPOSE 9089
ENTRYPOINT ["node", "/root/orc/bin/orc.js", "--disable-keyring", "--logs-stdout"]
CMD []
