'use strict';

const colors = require('colors/safe');
const async = require('async');
const kadence = require('@deadcanaries/kadence');
const ms = require('ms');
const bunyan = require('bunyan');
const RotatingLogStream = require('bunyan-rotating-file-stream');
const crypto = require('crypto');
const secp256k1 = require('secp256k1');
const levelup = require('levelup');
const leveldown = require('leveldown');
const encoding = require('encoding-down');
const dbcrypt = require('@adorsys/encrypt-down');
const keytar = require('keytar');
const assert = require('assert');

const { app } = require('electron');
const datadir = app.getPath('userData');
const orc = require('../lib');
const options = require('..//bin/config');
const path = require('path');
const fs = require('fs');
const ini = require('ini');
const config = fs.existsSync(path.join(datadir, 'config'))
  ? ini.parse(fs.readFileSync(path.join(datadir, 'config')).toString())
  : options(datadir);


// Persistent config overrides
config.NodeListenPort = '20088';
config.BridgePort = '20089';

// Write the config file if it doesn't exist
if (!fs.existsSync(path.join(datadir, 'config'))) {
  fs.writeFileSync(path.join(datadir, 'config'), ini.stringify(config));
}

// Per-run config overrides
config.TestNetworkEnabled = process.env.ORC_DESKTOP_MODE === 'testnet'
  ? '1'
  : '0';

let privkey, identity, logger, bridge, nonce, proof, secret;

kadence.constants.T_RESPONSETIMEOUT = ms('30s');

if (parseInt(config.TorUseSystemInstall)) {
  process.env.GRANAX_USE_SYSTEM_TOR = '1';
}

if (parseInt(config.TestNetworkEnabled)) {
  console.warn(`  ${colors.yellow.bold('WARNING:')}`,
    'ORC is running in test mode, difficulties are reduced');
  kadence.constants.IDENTITY_DIFFICULTY = kadence.constants.TESTNET_DIFFICULTY;
}

function writeCryptParams() {
  fs.writeFileSync(config.CryptParamsFilePath, crypto.randomBytes(32));
}

function readCryptParams() {
  if (fs.existsSync(config.CryptParamsFilePath)) {
    let params = fs.readFileSync(config.CryptParamsFilePath);
    return {
      salt: params.slice(0, 16),
      iv: params.slice(16)
    }
  } else {
    writeCryptParams();
    return readCryptParams();
  }
}

async function getSecret(salt) {
  return new Promise(async function(resolve, reject) {
    let result, password;

    try {
      password = await keytar.getPassword('ORC', config.PrivateKeyPath);
      assert.ok(password, 'not found');
      result = new orc.Secret(password, salt);
      resolve(result);
    } catch (err) {
      console.error(`  ${colors.bold.red('ERROR:')}`,
        `Failed to get decryption key from system, ${err.message}.`);
      reject(err);
    }
  });
}

async function _init(win) {
  const { salt } = readCryptParams();

  // Generate a private extended key if it does not exist
  if (!fs.existsSync(config.PrivateKeyPath)) {
    const prv = kadence.utils.generatePrivateKey();
    const pass = crypto.randomBytes(64).toString('base64');

    // Generate our passphrase to encrypt our new key
    await keytar.setPassword('ORC', config.PrivateKeyPath, pass);

    secret = await getSecret(salt);
    privkey = prv;

    fs.writeFileSync(config.PrivateKeyPath, secret.encrypt(prv));
  } else {
    const prv = fs.readFileSync(config.PrivateKeyPath);

    secret = await getSecret(salt);
    privkey = secret.decrypt(prv);
  }

  if (fs.existsSync(config.IdentityProofPath)) {
    proof = fs.readFileSync(config.IdentityProofPath);
  }

  if (fs.existsSync(config.IdentityNoncePath)) {
    nonce = parseInt(fs.readFileSync(config.IdentityNoncePath).toString());
  }

  // Initialize identity
  identity = new kadence.eclipse.EclipseIdentity(
    secp256k1.publicKeyCreate(privkey),
    nonce,
    proof
  );

  // If identity is not solved yet, start trying to solve it
  if (!identity.validate()) {
    console.warn(`  ${colors.bold.yellow('WARNING:')}`,
      'identity proof not yet solved, this can take a while');
    win.webContents.send('mining-proof-start-event');
    await identity.solve();
    win.webContents.send('mining-proof-finish-event');
    fs.writeFileSync(config.IdentityNoncePath, identity.nonce.toString());
    fs.writeFileSync(config.IdentityProofPath, identity.proof);
  }

  const logStreams = [{
    stream: new RotatingLogStream({
      path: config.LogFilePath,
      totalFiles: parseInt(config.LogFileMaxBackCopies),
      rotateExisting: true,
      gzip: true,
      period: '1d',
      threshold: '10m',
      totalSize: '30m'
    })
  }];

  // Initialize logging
  logger = bunyan.createLogger({
    name: identity.fingerprint.toString('hex'),
    streams: logStreams,
    level: parseInt(config.VerboseLoggingEnabled) ? 'debug' : 'info'
  });

  init(win);
}

async function init(win) {
  // Initialize public contact data
  const contact = {
    hostname: '127.0.0.1', // NB: Placeholder (tor plugin overrides this)
    protocol: 'http:',
    port: parseInt(config.NodeVirtualPort)
  };

  // Create a JSON Web Key for encrypting database
  const jwk = {
    kty: 'oct',
    alg: 'A256GCM',
    use: 'enc',
    k: `jwk${kadence.utils.hash160(privkey).toString('hex')}`
  };

  // Initialize network storage engine
  const storage = levelup(encoding(
    dbcrypt(leveldown(config.NetworkStorageDatabasePath), { jwk })
  ));

  // Initialize protocol implementation
  const node = new orc.Node({
    logger,
    contact,
    identity,
    privateKey: privkey,
    storage,
    hashcashDifficulty: parseInt(config.HashCashDifficulty),
    peerCachePath: config.PeerCacheStorageDatabasePath
  });

  // Handle any fatal errors
  node.on('error', (err) => {
    logger.error(err.message.toLowerCase());
  });

  // Establish onion hidden service
  node.plugin(kadence.onion({
    dataDirectory: config.NodeOnionServiceDataDirectory,
    virtualPort: config.NodeVirtualPort,
    localMapping: `127.0.0.1:${config.NodeListenPort}`,
    torrcEntries: {
      CircuitBuildTimeout: 10,
      KeepalivePeriod: 60,
      NewCircuitPeriod: 60,
      NumEntryGuards: 8,
      Log: `${config.TorLoggingVerbosity} stdout`
    },
    passthroughLoggingEnabled: !!parseInt(config.TorPassthroughLoggingEnabled),
    // NB: Defaulted to 4 because https://trac.torproject.org/projects/tor/ticket/29050
    socksVersion: parseInt(config.TorSocksProxyVersion)
  }));

  let bridgeOpts = {
    privateKeyPath: config.PrivateKeyPath,
    secret,
    cryptParams: readCryptParams(),
    logFilePath: config.LogFilePath,
    otpSecretFilePath: config.TwoFactorAuthSecretPath,
    storage: levelup(encoding(
      dbcrypt(leveldown(config.BridgeStorageDatabasePath), { jwk })
    )),
    syncPassphraseToKeychain: true
  };

  bridge = new orc.Bridge(node, bridgeOpts);

  logger.info(`establishing local bridge on port ${config.BridgePort}`);
  bridge.listen(parseInt(config.BridgePort), config.BridgeHostname);
  win.webContents.send('bridge-listen-event', parseInt(config.BridgePort));

  // Use verbose logging if enabled
  if (!!parseInt(config.VerboseLoggingEnabled)) {
    node.plugin(kadence.logger(logger));
  }

  // Cast network nodes to an array
  if (typeof config.NetworkBootstrapNodes === 'string') {
    config.NetworkBootstrapNodes = config.NetworkBootstrapNodes.trim().split();
  }

  async function joinNetwork(callback) {
    let entry = null;
    let peers = config.NetworkBootstrapNodes.concat(
      await node.peers.getBootstrapCandidates()
    );

    if (peers.length === 0) {
      logger.info('no bootstrap seeds provided and no known profiles');
      logger.info('running in seed mode (waiting for connections)');

      return node.router.events.once('add', (identity) => {
        config.NetworkBootstrapNodes = [
          kadence.utils.getContactURL([
            identity,
            node.router.getContactByNodeId(identity)
          ])
        ];
        joinNetwork(callback)
      });
    }

    logger.info(`joining network from ${peers.length} seeds`);
    win.webContents.send('joining-network-event');
    async.detectSeries(peers, (seed, done) => {
      let contact = kadence.utils.parseContactURL(seed);

      logger.info(`requesting identity information from ${seed}`);
      node.ping(contact, (err) => {
        if (err) {
          return done(null, false);
        }

        entry = [
          ...node.router.getClosestContactsToKey(node.identity)
        ].shift();

        node.join(contact, (err) => {
          done(null, (err ? false : true) && node.router.size > 1);
        });
      });
    }, (err, result) => {
      if (!result) {
        logger.error('failed to join network, will retry in 1 minute');
        callback(new Error('Failed to join network'));
      } else {
        callback(null, entry);
      }
    });
  }

  logger.info('bootstrapping tor and establishing hidden service');
  node.listen(parseInt(config.NodeListenPort), () => {
    logger.info(
      `node listening on local port ${config.NodeListenPort} ` +
      `and exposed at http://${node.contact.hostname}:${node.contact.port}`
    );

    function ensureConnected(callback) {
      node.router.events.removeListener('remove', _checkIfDisconnected);
      async.retry({
        times: Infinity,
        interval: 60000
      }, done => joinNetwork(done), (err, entry) => {
        if (err) {
          logger.error(err.message);
          callback(err);
        }

        logger.info(
          `connected to network via ${entry[0]} ` +
          `(http://${entry[1].hostname}:${entry[1].port})`
        );
        logger.info(`discovered ${node.router.size} peers from seed`);
        node.router.events.on('remove', _checkIfDisconnected);
        // Remove bootstrap seed if fingerprint was unknown
        node.router.removeContactByNodeId(
          '0000000000000000000000000000000000000000'
        );
        callback();
      });
    }

    function _checkIfDisconnected(fingerprint) {
      logger.debug('peer %s dropped from routing table', fingerprint);

      if (node.router.size === 0) {
        ensureConnected(() => null);
      }
    }

    ensureConnected(() => null);
  });
}

module.exports = _init;
