'use strict';


class BridgeClient {

  constructor(baseUrl) {
    this.url = baseUrl;
  }

  endpoint(path, csrf) {
    return `${this.url}/${path}` + (csrf ? `?_csrf=${csrf}` : '');
  }

  csrf(path = '') {
    return new Promise((resolve) => {
      const request = new XMLHttpRequest();
      request.withCredentials = true;
      request.open('GET', this.endpoint(path));
      request.setRequestHeader('Accept', 'application/json');
      request.send();

      request.onload = () => {
        let token;

        try {
          token = JSON.parse(request.responseText).meta.csrfToken;
        } catch (err) {
          resolve('');
        }

        resolve(token);
      };
    });
  }

  login(passphrase) {
    return new Promise(async (resolve, reject) => {
      const csrf = await this.csrf();
      const form = new FormData();
      form.append('passphrase', passphrase);

      const request = new XMLHttpRequest();
      request.withCredentials = true;
      request.open('POST', this.endpoint('login', csrf));
      request.setRequestHeader('Accept', 'application/json');
      request.send(form);

      request.onerror = reject;
      request.onload = () => {
        if (request.status !== 200) {
          return reject(new Error(JSON.parse(request.responseText).error));
        }

        resolve();
      };
    });
  }

  status() {
    return new Promise((resolve, reject) => {
      const request = new XMLHttpRequest();
      request.withCredentials = true;
      request.open('GET', this.endpoint(''));
      request.setRequestHeader('Accept', 'application/json');
      request.send();

      request.onerror = reject;
      request.onload = () => {
        if (request.status !== 200) {
          return reject(new Error(JSON.parse(request.responseText).error));
        }

        resolve(JSON.parse(request.responseText));
      };
    });
  }

  list() {
    return new Promise((resolve, reject) => {
      const request = new XMLHttpRequest();
      request.withCredentials = true;
      request.open('GET', this.endpoint('objects'));
      request.setRequestHeader('Accept', 'application/json');
      request.send();

      request.onerror = reject;
      request.onload = () => {
        if (request.status !== 200) {
          return reject(new Error(JSON.parse(request.responseText).error));
        }

        resolve(JSON.parse(request.responseText));
      };
    });
  }

  forget(fileKey) {
    return new Promise(async (resolve, reject) => {
      const csrf = await this.csrf(`objects/${fileKey}/info`);
      const request = new XMLHttpRequest();
      request.withCredentials = true;
      request.open('POST',
        this.endpoint('objects/' + fileKey + '/forget', csrf));
      request.setRequestHeader('Accept', 'application/json');
      request.send();

      request.onerror = reject;
      request.onload = () => {
        if (request.status !== 200) {
          return reject(new Error(JSON.parse(request.responseText).error));
        }

        resolve(JSON.parse(request.responseText));
      };
    });
  }

  upload(fileObject) {
    return new Promise(async (resolve, reject) => {
      const csrf = await this.csrf('objects');
      const request = new XMLHttpRequest();
      request.withCredentials = true;
      request.open('POST', this.endpoint('objects', csrf));
      request.setRequestHeader('Accept', 'application/json');

      const form = new FormData();
      form.append('file', fileObject, fileObject.name);

      request.send(form);

      request.onerror = reject;
      request.onload = function() {
        if (request.status !== 200) {
          return reject(new Error(JSON.parse(request.responseText).error));
        }

        resolve(JSON.parse(request.responseText));
      };
    });
  }

  download(key) {
    return new Promise((resolve, reject) => {
      const request = new XMLHttpRequest();
      request.withCredentials = true;
      request.responseType = 'blob';
      request.open('GET', this.endpoint('objects/' + key));
      request.setRequestHeader('Accept', 'application/json');
      request.send();

      request.onerror = reject;
      request.onload = () => {
        if (request.status !== 200) {
          return reject(new Error(JSON.parse(request.responseText).error));
        }

        resolve(URL.createObjectURL(request.response));
      };
    });
  }

  resolve(href) {
    return new Promise(async (resolve, reject) => {
      const csrf = await this.csrf('objects');
      const form = new FormData();
      form.append('href', href);

      const request = new XMLHttpRequest();
      request.withCredentials = true;
      request.open('POST', this.endpoint('objects/resolve', csrf));
      request.setRequestHeader('Accept', 'application/json');
      request.send(form);

      request.onerror = reject;
      request.onload = () => {
        if (request.status !== 200) {
          return reject(new Error(JSON.parse(request.responseText).error));
        }

        resolve(JSON.parse(request.responseText));
      };
    });
  }

  messages() {
    return new Promise((resolve, reject) => {
      const request = new XMLHttpRequest();
      request.withCredentials = true;
      request.open('GET', this.endpoint('messages'));
      request.setRequestHeader('Accept', 'application/json');
      request.send();

      request.onerror = reject;
      request.onload = () => {
        if (request.status !== 200) {
          return reject(new Error(JSON.parse(request.responseText).error));
        }

        resolve(JSON.parse(request.responseText));
      };
    });
  }

}

module.exports = BridgeClient;
