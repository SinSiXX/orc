const { app, Menu, BrowserWindow } = require('electron');
const path = require('path');

// Handle creating/removing shortcuts on Windows when installing/uninstalling.
if (require('electron-squirrel-startup')) { // eslint-disable-line global-require
  app.quit();
}

if (process.platform === 'linux') {
  process.env.GRANAX_USE_SYSTEM_TOR = '1';
}

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let win, deeplinkingUrl;

// Override the application name
app.setName('orc');

function createWindow() {
  // Create the browser window.
  win = new BrowserWindow({
    width: 400,
    minWidth: 400,
    maxWidth: 400,
    height: 600,
    minHeight: 600,
    show: false,
    icon: path.join(__dirname, 'assets/img/logo-app-icon.png')
  });

  // Don't show the menu bar
  Menu.setApplicationMenu(require('./menu'));

  // and load the index.html of the app.
  win.loadURL(`file://${__dirname}/index.html`);

  if (process.env.ORC_DESKTOP_MODE === 'debug') {
    win.toggleDevTools();
  }

  // Emitted when the window is closed.
  win.on('closed', () => {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    win = null;
  });

  win.webContents.on('did-finish-load', () => {
    // Windows - Linux: Handle deeplink url
    if (process.platform == 'win32' || process.platform == 'linux') {
      // Keep only command line / deep linked arguments
      deeplinkingUrl = process.argv ? process.argv[1] : null;
    }
    if (deeplinkingUrl) {
      setTimeout(() => {
        win.webContents.send('open-url-event', deeplinkingUrl);
      }, 1000);
    }
  });

  win.once('ready-to-show', () => {
    win.show();
  });
};

// Open orc:// links
app.setAsDefaultProtocolClient('orc');

// This method makes your application a Single Instance Application
// https://electronjs.org/docs/api/app#apphassingleinstancelock
const gotTheLock = app.requestSingleInstanceLock();

if (!gotTheLock) {
  app.quit();
} else {
  app.on('second-instance', (event, argv, workingDirectory) => {
    if (win) {
      // Windows - Linux: Handle deeplink url
      if (process.platform == 'win32' || process.platform == 'linux') {
        deeplinkingUrl = argv ? argv[1] : null;
        if (deeplinkingUrl) {
          win.webContents.send('open-url-event', deeplinkingUrl);
        }
      }
      // Someone tried to run a second instance, we should focus our window.
      if (win.isMinimized()) win.restore();
      win.focus();
    }
  });
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', () => {
  createWindow();

  if (process.env.ORC_DESKTOP_MODE !== 'debug') {
    require('./daemon')(win);
  }
});

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  // if (process.platform !== 'darwin') {
  app.quit();
  // }
});

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (win === null) {
    createWindow();
  }
});

app.on('open-url', function(e, url) {
  e.preventDefault();
  deeplinkingUrl = url;
  // Wait for main window to be ready
  if (win) {
    win.webContents.send('open-url-event', deeplinkingUrl);
    deeplinkingUrl = null;
    if (win.isMinimized()) {
      win.restore();
    } else {
      win.focus();
    }
  }
});
