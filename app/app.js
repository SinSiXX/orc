'use strict';

const {
  ORC_DESKTOP_MODE,
  ORC_DESKTOP_BRIDGE,
  ORC_DESKTOP_PASSWORD
} = process.env;

const { ipcRenderer } = require('electron');
const { app, clipboard, dialog } = require('electron').remote;

const { BlobPointer } = require('../lib');
const Client = require('./client');
const Vue = require('vue/dist/vue');
const keytar = require('keytar');

const ini = require('ini');
const path = require('path');
const fs = require('fs');
const configFile = path.join(app.getPath('userData'), 'config');
const config = fs.existsSync(configFile)
  ? ini.parse(fs.readFileSync(configFile).toString())
  : { PrivateKeyPath: path.join(app.getPath('userData'), 'orc_ecdsa') };

const url = ORC_DESKTOP_BRIDGE || (ORC_DESKTOP_MODE === 'debug'
  ? 'http://127.0.0.1:10089'
  : 'http://127.0.0.1:20089');
const bridge = new Client(url);


const polls = [];
const objects = [];
const peers = [];


const upload = new Vue({
  el: '#upload',
  data: {
    uploading: false,
    filename: '',
    logs: []
  },
  methods: {
    upload: function(event) {
      this.filename = event.target.files[0].name;
      this.uploading = true;
      bridge.upload(event.target.files[0]).then(
        (obj) => {
          this.uploading = false;
          this.filename = '';
          obj.downloading = false;
          objects.unshift(obj);
        },
        (e) => {
          console.error(e);
          dialog.showErrorBox('ORC Upload Failed :(', e.message);
          this.uploading = false;
          this.filename = '';
        }
      );
    }
  },
  mounted: function() {
    [
      'drag',
      'dragstart',
      'dragend',
      'dragover',
      'dragenter',
      'dragleave',
      'drop'
    ].forEach((evt) => {
      /*
        For each event add an event listener that prevents the default action
        (opening the file in the browser) and stop the propagation of the event (so
        no other elements open the file in the browser)
      */
      this.$refs.fileupload.addEventListener(evt, function(e) {
        e.preventDefault();
        e.stopPropagation();
      }, false);
    });
    this.$refs.fileupload.addEventListener('drop', (e) => {
      this.upload({ target: event.dataTransfer });
    });
  }
});

const header = new Vue({
  el: '#header',
  data: {
    href: '',
    valid: false,
    hint: false
  },
  mounted: function() {
    ipcRenderer.on('open-url-event', (_, url) => {
      this.href = url;
      this.resolve({ preventDefault: () => null });
    });
  },
  methods: {
    validate: function() {
      const [, url] = this.href.split('orc://');

      if (url && Buffer.from(url, 'hex').length === 76) {
        this.valid = true;
      } else {
        this.valid = false;
      }
    },
    resolve: function(e) {
      e.preventDefault();

      if (!this.valid) {
        this.hint = true;
        return setTimeout(() => this.hint = false, 250);
      }

      const href = this.href;
      const [key] = BlobPointer.parseHref(href);
      const placeholder = { resolving: true, filename: '', key, href };

      this.href = '';

      objects.unshift(placeholder);
      bridge.resolve(href).then(
        function() {
          refreshList().then(function() {
            objects.splice(objects.indexOf(placeholder), 1);
          });
        },
        function(e) {
          console.error(e.message);
          dialog.showErrorBox('ORC Lookup Failed :(', e.message);
          objects.splice(objects.indexOf(placeholder), 1);
        }
      );
    }
  }
});

const loader = new Vue({
  el: '#loading',
  data: {
    loading: true
  }
})

const files = new Vue({
  el: '#files',
  data: {
    objects
  },
  methods: {
    copy(obj) {
      clipboard.writeText(obj.href);
      (new Notification('ORC', {
        body: `Copied link for ${obj.filename} (${obj.key.substr(0, 6)})`,
        icon: path.join(__dirname, 'assets/img/logo-app-icon.png')
      }));
    },
    download(obj) {
      obj.downloading = true;
      bridge.download(obj.key).then((objurl) => {
        obj.downloading = false;
        triggerDownload(objurl, obj.filename);
      }, (e) => {
        obj.downloading = false;
        dialog.showErrorBox('ORC Download Failed :(', e.message);
      });
    },
    forget(obj) {
      if (!confirm(`Forget ${obj.filename} (${obj.key.substr(0, 6)})`)) {
        return;
      }
      objects.splice(objects.indexOf(obj), 1);
      bridge.forget(obj.key).then(
        function() {
          refreshList();
        },
        function(e) {
          console.error(e);
          dialog.showErrorBox('ORC Forget Failed :(', e.message);
        }
      );
    }
  }
});

const footer = new Vue({
  el: '#footer',
  data: {
    status: {
      peers,
      disconnected: true
    }
  },
  mounted: function() {
    ipcRenderer.on('joining-network-event', () => {
      this.status.disconnected = false;
    });

    if (process.env.ORC_DESKTOP_MODE === 'debug') {
      this.status.disconnected = false;
    }
  }
});

const connecting = new Vue({
  el: '#connecting',
  data: {
    message: 'Connecting to ORC daemon...',
    error: false,
    connected: false,
    enabled: true,
    statusClass: ''
  },
  methods: {
    reconnect: function() {
      this.login();
    },
    login: async function() {
      const passphrase = ORC_DESKTOP_PASSWORD || ORC_DESKTOP_MODE === 'debug'
        ? 'Simulation'
        : await keytar.getPassword('ORC', config.PrivateKeyPath);

      this.enabled = true;
      this.connected = false;
      this.error = false;
      this.message = 'Connecting to ORC daemon...';
      this.statusClass = '';

      polls.forEach(i => clearInterval(i));

      bridge.login(passphrase).then(
        () => {
          refreshStatus();
          refreshList().then(() => {
            this.connected = true;
            this.error = false;
            this.message = '';
            this.enabled = false;
          });
          polls.push(setInterval(refreshStatus, 60000));
          polls.push(setInterval(refreshList, 60000));
          polls.push(setInterval(refreshMessages, 4000));
        },
        (e) => {
          console.error(e);
          this.connected = false;
          this.error = true;
          this.message = e.message || 'Failed to connect to ORC daemon.';
          this.statusClass = 'bg-red';
        }
      );
    }
  },
  mounted: function() {
    // NB: if the user refreshes the app, we won't receive the
    // "bridge-listen-event", so if we don't receive that event after some
    // reasonable time, try login anyway
    const loginWait = setTimeout(() => this.login(), 3000);

    ipcRenderer.on('mining-proof-start-event', () => {
      this.enabled = true;
      this.error = false;
      this.connected = false;
      this.message = 'New network identity detected, mining proof. This can take a moment...'
    });

    ipcRenderer.on('mining-proof-finish-event', () => {
      this.message = 'Identity proof mined, waiting for bridge...';
    });

    ipcRenderer.on('bridge-listen-event', () => {
      clearTimeout(loginWait);
      this.login();
    });
  }
});

function triggerDownload(url, name) {
  const a = document.createElement('a');
  document.body.appendChild(a);
  a.style = 'display: none';
  a.href = url;
  a.download = name;
  a.click();
  document.body.removeChild(a);
}

function refreshStatus() {
  return new Promise((resolve, reject) => {
    bridge.status().then(
      function(status) {
        while (peers.length) {
          peers.pop();
        }
        status.peers.forEach(p => peers.push(p));
        resolve();
      },
      function(e) {
        console.error(e);
        while (peers.length) {
          peers.pop();
        }
        reject(e);
      }
    );
  });
}

function refreshList() {
  return new Promise((resolve, reject) => {
    bridge.list().then(
      function(result) {
        const resolving = [];
        while (objects.length) {
          const obj = objects.shift();
          if (obj.resolving) {
            resolving.push(obj);
          }
        }
        result.objects.forEach(obj => {
          obj.download = bridge.endpoint('objects/' + obj.key);
          obj.downloading = false;
          objects.push(obj)
        });
        resolving.forEach(obj => objects.unshift(obj));
        resolve();
      },
      function(e) {
        console.error(e);
        reject(e);
      }
    );
  });
}

function refreshMessages() {
  return new Promise((resolve, reject) => {
    bridge.messages().then(result => {
      const logs = result.logs.map(msg => {
        return msg.msg.charAt(0).toUpperCase() + msg.msg.slice(1);
      });

      while (upload.logs.length) {
        upload.logs.pop()
      }
      while (logs.length) {
        upload.logs.push(logs.shift());
      }
      resolve();
    }, reject);
  });
}
