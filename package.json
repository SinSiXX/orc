{
  "name": "@deadcanaries/orc",
  "productName": "ORC",
  "version": "15.0.0",
  "description": "Distributed and anonymous storage and publishing platform for journalists.",
  "main": "index.js",
  "bin": {
    "orc": "bin/orc.js"
  },
  "directories": {
    "test": "test",
    "lib": "lib",
    "doc": "doc"
  },
  "scripts": {
    "start-app": "electron-forge start",
    "test-app": "ORC_DESKTOP_MODE=testnet electron-forge start",
    "debug-app": "ORC_DESKTOP_MODE=debug electron-forge start",
    "package-app": "electron-forge package",
    "make-app": "electron-forge make",
    "start-sandbox": "docker-compose up --build --force-recreate --always-recreate-deps",
    "clean-sandbox": "docker rm $(docker ps -a -q --filter 'network=orc_default')",
    "test": "npm run unit-tests && npm run integration-tests && npm run e2e-tests && npm run linter",
    "unit-tests": "mocha --exit test/*.unit.js",
    "integration-tests": "mocha --exit test/*.integration.js",
    "e2e-tests": "mocha --exit test/*.e2e.js",
    "coverage": "istanbul cover _mocha -- --recursive --exit",
    "linter": "eslint ./index.js ./lib ./test ./bin",
    "generate-docs": "mkdir -p ./public/docs && rm -r ./public/docs && jsdoc lib -r -R README.md -u ./doc -c .jsdoc.json --verbose -d ./public/docs",
    "make-daemon": "mkdir -p out/make/@deadcanaries && node-deb --no-default-package-dependencies --install-strategy auto -- index.js logo.txt bin/ lib/ static/ views/ && mv orc_$(cat package.json | jq -cr .version)_all.deb orc-server_$(cat package.json | jq -cr .version)_all.deb && mv orc-server_$(cat package.json | jq -cr .version)_all.deb out/make/@deadcanaries",
    "make-all": "mkdir -p out && rm -r out && npm run make-app && npm run make-daemon"
  },
  "node_deb": {
    "package_name": "orc",
    "dependencies": "git, tor, nodejs, npm, build-essential, sudo, libsecret-1-dev",
    "templates": {
      "control": "debian/templates/control",
      "default_variables": "debian/templates/default",
      "executable": "debian/templates/executable",
      "postinst": "debian/templates/postinst",
      "postrm": "debian/templates/postrm",
      "systemd_service": "debian/templates/systemd.service",
      "sysv_init": "debian/templates/sysv-init",
      "upstart_conf": "debian/templates/upstart.conf"
    },
    "entrypoints": {
      "cli": "bin/orc.js --use-system-tor",
      "daemon": "bin/orc.js --datadir /etc/orc --passphrase /etc/orc/passwd --logs-stdout --use-system-tor"
    }
  },
  "config": {
    "forge": {
      "packagerConfig": {
        "icon": "app/assets/img/logo-app-icon.icns"
      },
      "makers": [
        {
          "name": "@electron-forge/maker-squirrel",
          "config": {
            "name": "ORC",
            "setupIcon": "app/assets/img/favicon.ico"
          }
        },
        {
          "name": "@electron-forge/maker-dmg",
          "platforms": [
            "darwin"
          ],
          "config": {
            "background": "app/assets/img/bg.jpg",
            "format": "ULFO",
            "icon": "app/assets/img/logo-app-icon.icns",
            "overwrite": true
          }
        },
        {
          "name": "@electron-forge/maker-deb",
          "config": {
            "maintainer": "Dead Canaries",
            "homepage": "https://orc.network",
            "description": "Onion Routed Cloud",
            "icon": "app/assets/img/logo-app-icon.png",
            "name": "orc-desktop",
            "productName": "ORC",
            "bin": "ORC",
            "depends": [
              "tor"
            ]
          }
        }
      ]
    }
  },
  "keywords": [
    "orc",
    "tor",
    "p2p",
    "kademlia",
    "cloud",
    "distributed",
    "private storage",
    "anonymous storage"
  ],
  "repository": {
    "type": "git",
    "url": "https://gitlab.com/deadcanaries/orc.git"
  },
  "author": "Emery Rose Hall <emery@deadcanaries.org>",
  "contributors": [
    "Dylan Lott <lott.dylan@gmail.com>",
    "Ryan Foran <ryanforan@gmail.com>"
  ],
  "license": "AGPL-3.0",
  "engines": {
    "node": ">=10.15.0"
  },
  "dependencies": {
    "@adorsys/encrypt-down": "=2.0.1",
    "@deadcanaries/hsv3": "=1.1.5",
    "@deadcanaries/kadence": "=6.1.6",
    "async": "=2.4.1",
    "bunyan": "=1.8.12",
    "bunyan-rotating-file-stream": "=1.6.3",
    "busboy": "=0.2.14",
    "colors": "=1.3.1",
    "commander": "=2.9.0",
    "cookie-parser": "=1.4.3",
    "cors": "=2.8.4",
    "csurf": "=1.10.0",
    "daemon": "=1.1.0",
    "electron-squirrel-startup": "=1.0.0",
    "encoding-down": "=6.0.1",
    "express": "=4.16.4",
    "form-data": "=2.3.1",
    "is-electron": "=2.2.0",
    "is-running": "=2.1.0",
    "keytar": "=4.4.1",
    "leveldown": "=4.0.2",
    "levelup": "=4.0.0",
    "merge": "=1.2.1",
    "mime-types": "=2.1.16",
    "mkdirp": "=0.5.1",
    "ms": "=2.1.1",
    "notp": "=2.0.3",
    "npid": "=0.4.0",
    "pug": "=2.0.3",
    "qr-image": "=3.2.0",
    "rc": "=1.2.8",
    "read": "=1.0.7",
    "secp256k1": "=3.2.2",
    "serve-static": "=1.13.2",
    "thirty-two": "=1.0.2",
    "vue": "=2.6.8"
  },
  "devDependencies": {
    "@electron-forge/cli": "^6.0.0-beta.34",
    "@electron-forge/maker-deb": "^6.0.0-beta.34",
    "@electron-forge/maker-dmg": "^6.0.0-beta.34",
    "@electron-forge/maker-rpm": "^6.0.0-beta.34",
    "@electron-forge/maker-squirrel": "^6.0.0-beta.34",
    "@electron-forge/maker-zip": "^6.0.0-beta.34",
    "chai": "^2.2.0",
    "coveralls": "^3.0.1",
    "electron": "^4.0.8",
    "eslint": "^5.15.0",
    "ink-docstrap": "git+https://gitlab.com/deadcanaries/docstrap.git",
    "istanbul": "^0.4.5",
    "jsdoc": "^3.6.3",
    "memdown": "^3.0.0",
    "mocha": "^5.2.0",
    "node-deb": "^0.10.7",
    "node-mocks-http": "=1.6.7",
    "proxyquire": "^1.7.3",
    "rimraf": "^2.6.1",
    "sinon": "^2.4.1"
  }
}
