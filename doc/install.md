> **Note!** This guide is for install ORC server daemon. To install the ORC 
> desktop application, download a package for your platform from the 
> [release page on GitLab](https://gitlab.com/deadcanaries/orc/tags).

### GNU/Linux

We provide Debian packages for Debian 10 (Buster), Ubuntu 18.10 (Cosmic), 
or equivalent newer releases. Packages can be acquired via our APT repository 
or by direct download.

Make sure you have the required packages to setup the repository source:

```
sudo apt install gnupg2 lsb-release software-properties-common curl
```

Install Node.js from the NodeSource repositories to make sure we get a recent 
enough version.

```
curl -sL https://deb.nodesource.com/setup_10.x | sudo bash -
sudo apt-get install -y nodejs
```

Fetch the release signing key, add the repository, update your package cache 
and install using our convenience script!

```
curl -s https://packagecloud.io/install/repositories/deadcanaries/orc/script.deb.sh | sudo bash
sudo apt install orc
```

> If you prefer to setup everything manually, [follow the manual setup instructions](https://packagecloud.io/deadcanaries/orc/install#manual-deb).

Follow the post installation prompts to encrypt your private key. ORC will be 
installed as a systemd, sysv-init, or upstart service depending on your system. 

Now you can access your node by running the ORC program from your 
applications menu! You may also access it by opening a browser and navigating 
to `http://127.0.0.1:9089`, or remotely by using  
[Tor Browser](https://torproject.org) and navigating to the onion address that 
is contained in the file `/etc/orc/bridge.tor/hidden_service/hostname`.

#### Direct Download

Alternatively you can 
[download a release package](https://gitlab.com/deadcanaries/orc/releases) from the 
latest tag. Download links for the package(s) are in the release notes for each 
tagged release. Once downloaded you can install by running:

```
sudo dpkg -i orc_x.x.x_all.deb
sudo apt install -f
```

#### Process Monitoring

You may discover later that you'd like to tweak some advanced settings. Just 
update your configuration as desired at `/etc/orc/config` and restart the 
service.

```
sudo systemctl restart orc.service # or...
sudo service orc restart # or...
sudo /etc/init.d/orc restart # ...depending on your system
```

Depending on the init system you are using, you'll check logs differently.¬

```
journalctl -u orc # systemd
tail -f /var/log/orc/debug.log # otherwise
```

### Using Docker

If you are on Mac OS or a GNU/Linux distribution that is not based on Debian, 
just install [Docker](https://www.docker.com/community-edition) and run the 
commands below:

```bash
docker pull deadcanaries/orc
mkdir ~/.config/orcd
docker run -it -v ~/.config/orcd:/root/.config/orcd deadcanaries/orc --reset-passphrase
```

This will generate the configuration files and encrypted private keys and exit. 
Now you can run the ORC daemon, supplying it with these files and publishing 
the web interface port to the host (if you wish to access it locally):

```
docker run -it -p 127.0.0.1:9089:9089 -v ~/.config/orcd:/root/.config/orcd deadcanaries/orc
```

You must run the container with the `-i` or `--interactive` flag so you can set 
and/or input your passphrase. There is also a command line option for `orc` 
that will non-interactively read the passphrase from a file, `--passphrase`. If 
you wish you run the container non-interactively. This is useful for running a 
node in the cloud remotely and letting it restart automatically if it exits or 
reboots:

```bash
echo 'MySecurePassphrase' > ~/.config/orc/passwd
docker run \
--restart always \
--volume ~/.config/orcd:/root/.config/orcd \
--publish 127.0.0.1:9089:9089 \
--tty --detach \
deadcanaries/orc --passphrase /root/.config/orc/passwd
```

Then you can access your node via it's authenticated onion service using 
[Tor Browser](https://www.torproject.org/download/download-easy.html.en). Note 
that in order to run the container detached with automatic restarts, you cannot 
use the `--interactive` flag and must use the `--passphrase` method shown 
above. Consult the Docker documentation for details on different options.

#### Automatic Security Updates

When running the ORC server installation with Docker, you can configure your 
node to periodically check for updates and automatically download the latest 
image and restart your node to make sure you are always running the latest 
stable release. Since you already have Docker installed, pull the 
image for [Watchtower](https://github.com/v2tec/watchtower) and run it.

```
docker pull v2tec/watchtower
docker run -d --name watchtower -v /var/run/docker.sock:/var/run/docker.sock v2tec/watchtower
```

Now, Watchtower will check for the latest stable images for running containers 
and automatically update them.
