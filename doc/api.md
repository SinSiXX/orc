This guide will show you how to use the bridge your node exposes as a simple 
API that applications can use to upload, download, delete, and list objects 
you have stored in the network as well as get status information. For the 
purposes of brevity for this guide, we are going to assume the following 
default configuration scheme that is used in the simulation environment and 
will use `curl` for the examples.

```ini
BridgeHostname = 127.0.0.1
BridgePort = 10089 # live mode default is usually 9089
```

> Note that in order to receive JSON payloads as responses from the API instead 
> of the web interface HTML, you must set the header 
> `Accept: application/json`. Oh, and **protip**, you can pipe your command to 
> `jq` to pretty print the result!

### POST /login

First you need to establish an authenticated session. This is done by 
submitting your passphrase and receiving a `Set-Cookie` token header. The form 
parameters are `passphrase`, `oath`, and `keepalive`.

The `oath` parameter is required is 2FA is enabled and should be a valid TOTP 
code.

The `keepalive` parameter is optional and is a human readable time frame to 
keep the session alive like "6h" or "30d".

```
curl \                                                                         
--verbose \
--header "Accept: application/json" \
--form "passphrase=Simulation" \
http://127.0.0.1:10089/login | jq
*   Trying 127.0.0.1...
* TCP_NODELAY set
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
  0     0    0     0    0     0      0      0 --:--:-- --:--:-- --:--:--     0* Connected to 127.0.0.1 (127.0.0.1) port 10089 (#0)
> POST /login HTTP/1.1
> Host: 127.0.0.1:10089
> User-Agent: curl/7.61.0
> Accept: application/json
> Content-Length: 155
> Content-Type: multipart/form-data; boundary=------------------------abfd4216430e6ee7
> 
} [155 bytes data]
< HTTP/1.1 200 OK
< X-Powered-By: Express
< Access-Control-Allow-Origin: *
< Set-Cookie: token=234ec3b4-27c7-4a0a-aa4a-79a71aaa6ad7; Max-Age=900; Path=/; Expires=Tue, 14 Aug 2018 15:16:53 GMT; HttpOnly
< Vary: Accept
< Content-Type: application/json; charset=utf-8
< Content-Length: 2
< ETag: W/"2-vyGp6PvFo4RvsFtPoIWeCReyIC8"
< Date: Tue, 14 Aug 2018 15:01:53 GMT
< Connection: keep-alive
< 
{ [2 bytes data]
100   157  100     2  100   155      4    372 --:--:-- --:--:-- --:--:--   377
* Connection #0 to host 127.0.0.1 left intact
{}
```

Note the value of the `Set-Cookie` header. You'll be including this in every 
other request.

### GET /logout

Invalidates the current session.

```
curl \                                                                         
--cookie "token=234ec3b4-27c7-4a0a-aa4a-79a71aaa6ad7" \
http://127.0.0.1:10089/logout
```

### GET /

Fetches general status information about the running node.

```
curl \                                                                         
--header "Accept: application/json" \
--cookie "token=234ec3b4-27c7-4a0a-aa4a-79a71aaa6ad7" \
http://127.0.0.1:10089/ | jq     
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  1704  100  1704    0     0   554k      0 --:--:-- --:--:-- --:--:--  554k
{
  "peers": [
    [
      "78605de8b6b06fc834801dde47e12415c51efa12",
      {
        "hostname": "l466qntstik7falkkzxttgrtlu4u7g6yaxmyvjs2qamlzymsdsiqxdqd.onion",
        "protocol": "http:",
        "port": "80",
        "pubkey": "...",
        "proof": "..."
      }
    ],
    [
      "3f9e8519d66ba37f70a4e9fc2ef9d235480de5ab",
      {
        "hostname": "2s2u2o5fmk2lhdclsrfpfo4igzh7nwmpmfbsvucyvepvwehw2sxdyvad.onion",
        "protocol": "http:",
        "port": "80",
        "pubkey": "...",
        "proof": "..."
      }
    ],
    [
      "b5141334673315c5872f702eb9f6c186c15ee722",
      {
        "hostname": "fjk3xswbpeezfbtwv76rue5a644hp3vsbjnrbb6cczmo3anv7qsq2vyd.onion",
        "protocol": "http:",
        "port": "80",
        "pubkey": "...",
        "proof": "..."
      }
    ]
  ],
  "identity": "7d7e1bf0083d5dc74983e263a2a7cebaf898db65",
  "contact": {
    "hostname": "zeatlf6whhi2ngtrtjhhvxpexqqngxjg37ih6maaabzepeohktozm4yd.onion",
    "protocol": "http:",
    "port": "80",
    "pubkey": "...",
    "proof": "..."
  },
  "versions": {
    "protocol": "4.0.0",
    "software": "12.0.1"
  },
  "category": "status"
}
```

### GET /peers

Fetches a JSON array of all identities and their associated profiles for which 
the node has interacted with.

```
curl \                                                                         
--header "Accept: application/json" \
--cookie "token=234ec3b4-27c7-4a0a-aa4a-79a71aaa6ad7" \
http://127.0.0.1:10089/peers | jq
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  2222  100  2222    0     0   120k      0 --:--:-- --:--:-- --:--:--  120k
[
  {
    "protocol": "http:",
    "hostname": "2s2u2o5fmk2lhdclsrfpfo4igzh7nwmpmfbsvucyvepvwehw2sxdyvad.onion",
    "port": 80,
    "pubkey": "...",
    "proof": "..."
    "identity": "3f9e8519d66ba37f70a4e9fc2ef9d235480de5ab"
  },
  // ...
]
```

### GET /peers/{identity}

Given a known identity key, fetch the specific profile associated.

```
curl \                                                                         
--header "Accept: application/json" \
--cookie "token=234ec3b4-27c7-4a0a-aa4a-79a71aaa6ad7" \
http://127.0.0.1:10089/providers/78605de8b6b06fc834801dde47e12415c51efa12 | jq
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   560  100   560    0     0  46666      0 --:--:-- --:--:-- --:--:-- 50909
{
  "protocol": "http:",
  "hostname": "l466qntstik7falkkzxttgrtlu4u7g6yaxmyvjs2qamlzymsdsiqxdqd.onion",
  "port": 80,
  "pubkey": "...",
  "proof": "...",
  "identity": "78605de8b6b06fc834801dde47e12415c51efa12"
}
```

### POST /objects

You can upload a file to the network my sending a multipart/form-upload request 
to `POST /objects`. This works the same as if using a `<input type="file"/>` on 
a web page. 

```
curl \                                                                         
--header "Accept: application/json" \
--cookie "token=0eba09e1-f7fa-4a9a-a7bc-d9b65ec4db87" \
--form "file=@Pictures/avatar.jpg;type=image/jpeg" \
--form "policy=::RETRIEVE" \
http://127.0.0.1:10089/objects | jq
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 70243  100  5778  100 64465    562   6273  0:00:10  0:00:10 --:--:--  1420
{
  "filename": "avatar.jpg",
  "mimetype": "image/jpeg",
  "hashes": [
    "08b37ae500412cce8b7c4d4dd0045c275fdf5708",
    "695389c05335c26f259ab770213368778ddd8cb4",
    "61dd32d2fbc4c55c3e2473c810be526d6f0993ea"
  ],
  "href": "...",
  "key": "0000000000000000000000000000000000000000"
}
```

### GET /objects

Retreive a JSON list of your objects stored in the network and managed by this 
node. Each item returned in the list contains metadata regarding the type, 
size, name, hash, location of shards, and more.

```
curl \                                                                         
--header "Accept: application/json" \
--cookie "token=0eba09e1-f7fa-4a9a-a7bc-d9b65ec4db87" \
http://127.0.0.1:10089/objects | jq
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  5802  100  5802    0     0   566k      0 --:--:-- --:--:-- --:--:--  566k
[
  {
    "filename": "avatar.jpg",
    "mimetype": "image/jpeg",
    "hashes": [
      "08b37ae500412cce8b7c4d4dd0045c275fdf5708",
      "695389c05335c26f259ab770213368778ddd8cb4",
      "61dd32d2fbc4c55c3e2473c810be526d6f0993ea"
    ],
    "href": "...",
    "key": "0000000000000000000000000000000000000000"
  }
]
```

### GET /objects/{id}/info

Retrieve the metadata for a specific object by it's unique ID.

```
curl \                                                                    130 ↵
--header "Accept: application/json" \
--cookie "token=0eba09e1-f7fa-4a9a-a7bc-d9b65ec4db87" \
http://127.0.0.1:10089/objects/5b72f32bc787d2012e5c8f50/info | jq
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 13984  100 13984    0     0   758k      0 --:--:-- --:--:-- --:--:--  803k
{
  "filename": "avatar.jpg",
  "mimetype": "image/jpeg",
  "hashes": [
    "08b37ae500412cce8b7c4d4dd0045c275fdf5708",
    "695389c05335c26f259ab770213368778ddd8cb4",
    "61dd32d2fbc4c55c3e2473c810be526d6f0993ea"
  ],
  "href": "...",
  "key": "0000000000000000000000000000000000000000"
}
```

### GET /objects/{id}

You can download a file from the network knowing only the object's key in the 
local bridge service. The appropriate headers for content type are sent to 
enable browsers and other applications to display the downloaded object.

```
curl \                                                                         
--header "Accept: application/json" \
--cookie "token=0eba09e1-f7fa-4a9a-a7bc-d9b65ec4db87" \
http://127.0.0.1:10089/objects/5b72f32bc787d2012e5c8f50 > avatar.jpg
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 64172  100 64172    0     0  11069      0  0:00:05  0:00:05 --:--:-- 13965
```

### POST /objects/resolve

To fetch an object pointer shared by someone else, you can send the orc link
and get back an object pointer with an ID you can use in the download example.

```
curl \                                                                         
--header "Accept: application/json" \
--cookie "token=0eba09e1-f7fa-4a9a-a7bc-d9b65ec4db87" \
--form "href=orc://000000000000000000000000000000000000000000000..." \
http://127.0.0.1:10089/objects/resolve | jq
```

### POST /objects/{id}/forget

You can destroy an object, nullifying associated contracts. Returns a 200 and 
empty object.

```
curl \                                                                         
--header "Accept: application/json" \
--cookie "token=0eba09e1-f7fa-4a9a-a7bc-d9b65ec4db87" \
--request POST \
http://127.0.0.1:10089/objects/5b72f574c787d2012e5c8f81/delete | jq
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100     2  100     2    0     0     90      0 --:--:-- --:--:-- --:--:--    90
{}
```

### GET /messages

Requests the last ~200 log messages for debugging and status purposes.

```
curl \                                                                         
--header "Accept: application/json" \
--cookie "token=e1232b60-87f4-4407-af58-4522ce631d1b" \
http://127.0.0.1:10089/messages | jq
```

### POST /passphrase

You can reset your passphrase for accessing your node. This will reset all 
active sessions and you must login again.

```
curl \                                                                    
--header "Accept: application/json" \
--cookie "token=e1232b60-87f4-4407-af58-4522ce631d1b" \
--form "passphrase_old=Simulation" \
--form "passphrase_new=NewPassword" \
http://127.0.0.1:10089/passphrase | jq
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   275  100     2  100   273      1    225  0:00:02  0:00:01  0:00:01   226
{}
```
