This guide will cover settings up ORC for local development and testing.

### Setup Dependencies

First, setup your development environment. Install [Node.js](https://nodejs.org) 
for your platform and build dependencies.

#### Debian >= 10 / Ubuntu >= 18.04

Make sure you have the required packages to setup the repository source:

```
sudo apt install gnupg2 lsb-release software-properties-common curl
```

Install Node.js from the NodeSource repositories to make sure we get a recent 
enough version.

```
curl -sL https://deb.nodesource.com/setup_10.x | sudo bash -
sudo apt-get install -y nodejs
```

Install other development dependencies.

```
sudo apt install build-essential libsecret-1-dev tor git
```

#### MacOS

Download and install Node.js from the website. Then, in Terminal.app:

```
xcode-select --install
```

#### Windows

> Windows is not yet supported (maybe ever) and may not build at all.

```
npm install windows-build-tools --global
```

### Clone and Build

Clone this repository and install the dependencies:

```
git clone https://gitlab.com/deadcanaries/orc
cd orc
npm install
```

> Note that the application and the server daemon use different runtimes, so 
> it's necessary to rebuild native modules when switching from testing the app 
> to the daemon using `npm rebuild`.

### Setup Test Network

The best way to work on ORC, is to use the included network 
simulation script. Install [Docker](https://www.docker.com/) and 
[Docker Compose](https://docs.docker.com/compose/install/).

On Debian-based systems:

```
sudo apt install docker docker-compose
```

Make sure you add your user to the Docker group:

```
sudo usermod -aG docker $USER
```

Now you can run the sandbox!

```
npm run start-sandbox
```

This will volume mount the the appropriate directories for development, and 
then boots up a complete sandboxed ORC network, including a complete sandboxed 
Tor network and once bootstrapped, binds ports `10089`, `11089`, `12089`, 
`13089`, and `14089` to the host for full end-to-end testing using 5 nodes in 
a trusted grid. Each of these nodes can be controlled in your web browser by 
visiting their respective ports. 

The passphrase for each running node is `Simulation` for accessing the web 
interfaces. You can also access the API (see {@tutorial api}).

### Connect App To Sandbox

Give the sandbox moment to bootstrap and then run the ORC application in debug mode:

```
npm run debug-app
```

The development container does not persist state between 
runs. Note that stable releases are tagged and the `master` branch may contain 
unstable or bleeding-edge code. Happy hacking!
