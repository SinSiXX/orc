This guide will show you how to get started with running ORC! An ORC 
node requires a configuration file to get up and running. The path to this 
file is given to `orc` when starting a node.

```
orc --config path/to/orc.config
```

If a configuration file is not supplied, a minimal default configuration is 
automatically created and used, which will generate everything needed to run. 
All of this data will be created and stored in `$HOME/.config/orc`.

#### DaemonPidFilePath

##### Default: `$HOME/.config/orc/orc.pid`

The location to write the PID file for the daemon.

#### PrivateKeyPath

##### Default: `$HOME/.config/orc/orc_ecdsa`

Path to private key file to use for identity and encryption of private data.

#### CryptParamsFilePath

##### Default: `$HOME/.config/orc/crypt_params`

The path to read (or create on first run), the file that contains the unique 
salt used for key derivation and the initialization vector user for the 
encryption of secrets. **Losing or destroying this file will lose access to 
your node and prevent decryption of files. Back it up offline!**

#### NodeOnionServiceDataDirectory

##### Default: `$HOME/.config/orc/node_hs`

The path to the directory to instruct Tor to use for storing hidden service 
keys and other information.

#### NodeVirtualPort

##### Default: `80`

Sets the virtual port number for your node's RPC onion service.

#### NodeListenPort

##### Default: `9088`

Sets the local port to bind the node's RPC service.

#### VerboseLoggingEnabled

##### Default: `1`

More detailed logging of messages sent and received. Useful for debugging.

#### LogFilePath

##### Default: `$HEAD/.config/orc.log`

Path to write the daemon's log file. Log file will rotate either every 24 hours 
or when it exceeds 10MB, whichever happens first.

#### LogFileMaxBackCopies

##### Default: `3`

Maximum number of rotated log files to keep.

#### NetworkBootstrapNodes[]

##### Default: `http://z2ybz7kjxjtfiwcervfh376swy4je3ye4yne2atoi727634qzjonk7id.onion:80`

Add a map of network bootstrap nodes to this section to use for discovering 
other peers. Default configuration should come with a list of known and 
trusted contacts.

#### BridgeHostname

##### Default: `127.0.0.1`

Sets the hostname or IP address to which the bridge service should be bound. It 
is important to set this value to a loopback address if authentication is 
disabled to prevent others from accessing your objects.

#### BridgePort

##### Default: `9089`

Set the TCP port to which the bridge service's HTTP API should be bound.

#### TorPassthroughLoggingEnabled

##### Default: `0`

Redirects the Tor process log output through ORC's logger for the purpose of 
debugging.

#### TorLoggingVerbosity

##### Default: `notice`

Defines the verbosity level of the Tor process logging. Valid options are: `debug`, `info`, `notice`.

#### TorSocksProxyVersion

##### Default: `4`

Sets the SOCKS proxy type for Tor communication. This currently defaults to 4 
until [this ticket](https://trac.torproject.org/projects/tor/ticket/29050) is 
closed.

#### IdentityNoncePath

##### Default: `$HOME/.config/orc/eq_nonce`

Path to the file that contains the identity nonce.

#### IdentityProofPath

##### Default: `$HOME/.config/orc/eq_proof`

Path to the file that contains the identity proof.

#### TwoFactorAuthSecretPath

##### Default: `$HOME/.config/orc/otp_secret`

Path to store the encrypted 2FA auth secret for the API.

#### PassphraseFile

##### Default: (none)

Path to passphrase file to unlock ORC non-interactively.

#### NetworkStorageDatabasePath

##### Default: `$HOME/.config/orc/network.crypt`

Path to store the encrypted slices database. This contains data stored for 
others.

#### BridgeStorageDatabasePath

##### Default: `$HOME/.config/orc/bridge.crypt`

Path to store local encrypted pointer information for the API.

#### PeerCacheStorageDatabasePath

##### Default: `$HOME/.config/orc/peercache`

File path to track known peer information.

#### HashCashDifficulty

##### Default: `4`

Number of leading zeroes in hashcash stamps for storing blobs. Used for 
mitigating spam.
