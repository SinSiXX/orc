'use strict';

const { expect } = require('chai');
const Secret = require('../lib/secret');


describe('@class Secret', function() {

  let salt = Buffer.from('87ec4dae2835b8635aa5ad996798db1b', 'hex');
  let secret;
  let cleartext = 'a well kept secret';
  let encrypted;

  before(function(done) {
    Secret.request(null, 'KeyboardCat', salt).then(s => {
      secret = s;
      done();
    });
  });

  describe('@method encrypt', function() {

    it('should encrypt the cleartext with the secret', function() {
      encrypted = secret.encrypt(cleartext);
      expect(encrypted.toString('hex')).to.equal(
        '8bd4110deb58c143a3e1dc4ac4e3b059190a5ed6c5a00149eafa5ff9752fa31a'
      );
    });

  });

  describe('@method decrypt', function() {

    it('should decrypt the ciphertext with the secret', function() {
      expect(secret.decrypt(encrypted).toString()).to.equal(
        'a well kept secret'
      );
    });

  });

});
