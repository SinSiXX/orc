'use strict';

const { expect } = require('chai');
const sinon = require('sinon');
const kadence = require('@deadcanaries/kadence');
const { KademliaNode } = kadence;
const Node = require('../lib/node');
const bunyan = require('bunyan');
const secp256k1 = require('secp256k1');
const levelup = require('levelup');
const encoding = require('encoding-down');
const memdown = require('memdown');


function generatePrivateKey() {
  const privateKey = kadence.utils.generatePrivateKey();
  const publicKey = secp256k1.publicKeyCreate(privateKey);

  return { privateKey, publicKey };
}

function createNode(opts, NodeConstructor) {
  let Ctor = NodeConstructor || Node;

  const key = generatePrivateKey();
  const node = new Ctor({
    storage: levelup(encoding(memdown())),
    logger: bunyan.createLogger({ name: 'node-test', level: 'fatal' }),
    identity: new kadence.eclipse.EclipseIdentity(key.publicKey),
    privateKey: key.privateKey
  });

  return node;
}

describe('@class Node', function() {

  before((done) => {
    done();
  });

  describe('@constructor', function() {

    const sandbox = sinon.sandbox.create();

    after(() => {
      sandbox.restore();
    });

    it('should mount plugins', function() {
      const plugin = sandbox.spy(Node.prototype, 'plugin');
      const node = createNode({});
      expect(node).to.be.instanceOf(Node);
      expect(plugin.callCount).to.equal(5);
    });

  });

  describe('@method listen', function() {

    const sandbox = sinon.sandbox.create();

    after(() => {
      sandbox.restore();
    });

    it('should mount the protocol handlers and bind network', function() {
      const use = sinon.spy(Node.prototype, 'use');
      const listen = sandbox.stub(KademliaNode.prototype, 'listen');
      const node = createNode({});
      node.listen(0);
      expect(use.calledWithMatch('STORE')).to.equal(true);
      expect(listen.called).to.equal(true);
    });

  });

});
