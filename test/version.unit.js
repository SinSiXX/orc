'use strict';

const proxyquire = require('proxyquire').noPreserveCache();
const { expect } = require('chai');


describe('@module version', function() {

  it('should return the package version for software', function() {
    const v = proxyquire('../lib/version', {});
    expect(v.software).to.equal(require('../package').version);
  });

  describe('@method toString', function() {
    const v = proxyquire('../lib/version', {});
    expect(typeof v.toString()).to.equal('string');
  });

});
