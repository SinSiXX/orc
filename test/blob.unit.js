'use strict';

const { expect } = require('chai');
const { randomBytes } = require('crypto');
const constants = require('../lib/constants');
const { BlobPointer, BlobMapping } = require('../lib/blob');


describe('@class BlobMapping', function() {

  describe('@constructor', function() {

    it('should auto generate cryptparams if not supplied', function() {
      const mapping = new BlobMapping('test.bin');
      expect(mapping.params.password).to.have.lengthOf(32);
      expect(mapping.params.salt).to.have.lengthOf(8);
      expect(mapping.params.iv).to.have.lengthOf(16);
    });

    it('should create a mapping from a stream', function(done) {
      this.timeout(6000);
      const mapping = new BlobMapping('test.bin');
      const file = randomBytes(constants.UNIFORM_BLOB_SIZE * 4);
      let match = Buffer.from([]);
      mapping.on('finish', () => {
        expect(mapping.slices.size).to.equal(6);
        mapping
          .on('data', data => match = Buffer.concat([match, data]))
          .on('end', () => {
            expect(Buffer.compare(file, match)).to.equal(0);
            done();
          });
      });
      mapping.end(file);
    });

  });

  describe('@static fromBlobPointer', function() {

    it('should return a mapping from a pointer', function() {
      const pointer = new BlobPointer('test.bin', [
        Buffer.alloc(20, 0),
        Buffer.alloc(20, 1),
        Buffer.alloc(20, 2)
      ]);
      const mapping = BlobMapping.fromBlobPointer(pointer);
      expect(mapping.slices.size).to.equal(3);
    });

  });

  describe('@method toBlobPointer', function() {

    it('should create a pointer from the mapping', function(done) {
      const mapping = new BlobMapping('test.bin');
      const file = randomBytes(constants.UNIFORM_BLOB_SIZE * 2);
      mapping.on('finish', () => {
        const pointer = mapping.toBlobPointer();
        expect(pointer.hashes).to.have.lengthOf(4);
        done();
      });
      mapping.end(file);
    });

  });

  describe('@method writeToSliceMap', function() {

    it('should write the buffer to the slice map', function() {
      const mapping = BlobMapping.fromBlobPointer(new BlobPointer(
        'test.bin',
        [Buffer.from('5e52fee47e6b070565f74372468cdc699de89107', 'hex')]
      ));
      mapping.writeToSliceMap(Buffer.from('test'));
      expect(mapping.slices.get('5e52fee47e6b070565f74372468cdc699de89107'))
        .to.have.lengthOf(4)
    });

  });

});
