'use strict';

const sinon = require('sinon');
const { EventEmitter } = require('events');
const http = require('http');
const { expect } = require('chai');
const Transport = require('../lib/transport');


describe('@class Transport', function() {

  describe('@private _createRequest', function() {

    it('should return a http request object', function() {
      const transport = new Transport();
      const request = transport._createRequest({});
      request.on('error', () => null); // noop
      expect(request).to.be.instanceOf(http.ClientRequest);
    });

  });

  describe('@private _createServer', function() {

    it('should return a https server object', function() {
      const transport = new Transport();
      const server = transport._createServer();
      expect(server).to.be.instanceOf(http.Server);
    });

    it('should disable nagle on connection', function(done) {
      const transport = new Transport();
      const server = transport._createServer();
      const setNoDelay = sinon.stub();
      const socket = new EventEmitter();
      socket.setNoDelay = setNoDelay;
      server.removeListener('connection',
        server.listeners('connection').shift());
      server.emit('connection', socket);
      setImmediate(() => {
        expect(setNoDelay.calledWithMatch(true)).to.equal(true);
        done();
      });
    });

  });

});
