'use strict';

const { expect } = require('chai');
const async = require('async');
const netgen = require('./fixtures/node-generator');
const { BlobPointer, BlobMapping } = require('../lib/blob');
const { randomBytes } = require('crypto');


describe('@module orc (end-to-end)', function() {

  const NUM_NODES = 4;
  const nodes = [];

  before(function(done) {
    this.timeout(12000);
    netgen(NUM_NODES, (n) => {
      n.forEach((node) => nodes.push(node));
      async.eachSeries(nodes, (n, done) => {
        n.listen(n.contact.port, n.contact.hostname, done)
      }, done);
    });
  });

  after(function(done) {
    this.timeout(12000);
    setTimeout(() => {
      async.each(nodes, (n, next) => {
        n.transport.server.close();
        next();
      }, done);
    }, 4000);
  });

  it('should join all nodes together', function(done) {
    this.timeout(240000);
    async.eachOfSeries(nodes, (n, i, next) => {
      if (i === 0) {
        next();
      } else {
        n.join([
          nodes[0].identity.toString('hex'),
          nodes[0].contact
        ], () => next());
      }
    }, () => {
      nodes[nodes.length - 1].router.getClosestContactsToKey(
        nodes[nodes.length - 1].identity.toString('hex'),
        20
      );
      nodes.forEach((n) => {
        expect(n.router.size > 0.75 / NUM_NODES).to.equal(true);
      });
      done();
    });
  });

  let mapping, pointer, href, file = randomBytes(4096);

  it('should upload the mapping to the network', function(done) {
    this.timeout(124000);
    mapping = new BlobMapping('document.pdf');
    mapping.on('finish', function() {
      nodes[0].upload(mapping).then(function(p) {
        pointer = p;
        href = p.toEncryptedSlice().href;
        done();
      }, done);
    });
    mapping.end(file);
  });

  it('should resolve a pointer from a href', function(done) {
    this.timeout(124000);
    nodes[1].resolve(href).then(function(p) {
      expect(p).to.be.instanceOf(BlobPointer);
      done();
    }, done);
  });

  it('should download the mapping from the network', function(done) {
    this.timeout(124000);
    nodes[2].download(pointer).then(function(m) {
      let pdf = '';
      m.on('data', d => pdf += d.toString('hex')).on('end', () => {
        expect(pdf).to.equal(file.toString('hex'));
        done();
      });
    }, done);
  });

});
