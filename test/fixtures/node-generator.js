'use strict';

const async = require('async');
const bunyan = require('bunyan');
const orc = require('../../index');
const kadence = require('@deadcanaries/kadence');
const secp256k1 = require('secp256k1');
const levelup = require('levelup');
const memdown = require('memdown');
const encoding = require('encoding-down');

let startPort = 45000;

kadence.constants.IDENTITY_DIFFICULTY = kadence.constants.TESTNET_DIFFICULTY;
kadence.constants.T_RESPONSETIMEOUT = 12000; // Cloud CI/CD is sloooooooooow


module.exports = function(numNodes, callback) {

  const nodes = [];

  function createNode(callback) {
    const contact = {
      hostname: 'localhost',
      port: startPort++,
      protocol: 'http:'
    };
    const privateKey = kadence.utils.generatePrivateKey();
    const identity = new kadence.eclipse.EclipseIdentity(
      secp256k1.publicKeyCreate(privateKey)
    );
    const storage = levelup(encoding(memdown(identity.pubkey)));

    identity.solve().then(identity => {
      const logger = bunyan.createLogger({
        levels: ['fatal'],
        name: identity.fingerprint.toString('hex')
      });

      callback(new orc.Node({
        privateKey,
        contact,
        logger,
        identity,
        storage,
        hashcashDifficulty: 0
      }));
    });
  }

  async.times(numNodes, function(n, done) {
    createNode((node) => {
      nodes.push(node);
      done();
    });
  }, () => callback(nodes));
};
