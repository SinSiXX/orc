'use strict';

const path = require('path');
const { tmpdir } = require('os');
const crypto = require('crypto');
const { expect } = require('chai');
const sinon = require('sinon');
const http = require('http');
const Bridge = require('../lib/bridge');
const Node = require('../lib/node');
const FormData = require('form-data');
const kadence = require('@deadcanaries/kadence');
const bunyan = require('bunyan');
const { utils: keyutils } = kadence;
const Secret = require('../lib/secret');
const fs = require('fs');
const { totp } = require('notp');
const base32 = require('thirty-two');
const secp256k1 = require('secp256k1');
const levelup = require('levelup');
const encoding = require('encoding-down');
const memdown = require('memdown');


describe('@class Bridge (integration)', function() {

  let sandbox = sinon.sandbox.create();

  let otpSecretFilePath = path.join(tmpdir(),
    `otpsecret-${crypto.randomBytes(16).toString('hex')}`);
  let privateKeyPath = path.join(tmpdir(),
    `xprv-${crypto.randomBytes(16).toString('hex')}`);
  let cryptParams = {
    iv: crypto.randomBytes(16),
    salt: crypto.randomBytes(16)
  };
  let secret = new Secret('Keyboard Cat', cryptParams.salt);
  let node = null;
  let bridge = null;
  let port = 0;
  let oathSecret = null;
  let csrfToken = '';
  let cookies = [];

  before(function(done) {
    this.timeout(6000);
    const prv = keyutils.generatePrivateKey();
    fs.writeFileSync(privateKeyPath, secret.encrypt(prv))
    node = new Node({
      logger: bunyan.createLogger({
        name: 'bridge/integration/logger',
        level: 'fatal'
      }),
      storage: levelup(encoding(memdown())),
      identity: new kadence.eclipse.EclipseIdentity(
        secp256k1.publicKeyCreate(prv))
    });
    bridge = new Bridge(node, {
      storage: levelup(encoding(memdown())),
      privateKeyPath,
      cryptParams,
      otpSecretFilePath
    });
    bridge.listen(0, () => {
      port = bridge.server.address().port;
      getAndUpdateCsrfSessionAndToken('/', done);
    });
  });

  after(() => {
    sandbox.restore();
    bridge.server.close();
  });

  function cookie() {
    return cookies.join(';');
  };

  function getAndUpdateCsrfSessionAndToken(path = '/', done) {
    let req = http.request({
      hostname: 'localhost',
      port,
      path: path,
      method: 'GET',
      headers: {
        Accept: 'application/json',
        Cookie: cookie()
      }
    }, (res) => {
      let body = '';
      res.on('data', data => body += data.toString());
      res.on('end', () => {
        body = JSON.parse(body);
        csrfToken = body.csrfToken || body.meta.csrfToken || csrfToken;
        cookies = cookies.concat(res.headers['set-cookie']);
        done(null, body);
      });
    });
    req.end();
  }


  it('should login and get the session token', function(done) {
    let form = new FormData();
    form.append('passphrase', 'Keyboard Cat');
    form.submit({
      hostname: 'localhost',
      port,
      path: `/login?_csrf=${csrfToken}`,
      method: 'POST',
      headers: {
        Accept: 'application/json',
        Cookie: cookie(),
      }
    }, (err, res) => {
      expect(res.statusCode).to.equal(200);
      cookies = cookies.concat(res.headers['set-cookie']);
      done();
    });
  });

  it('should enable two factor authentication', function(done) {
    getAndUpdateCsrfSessionAndToken('/totp', (err, body) => {
      oathSecret = base32.decode(body.totp.secret);
      let form = new FormData();
      form.append('oath', totp.gen(oathSecret));
      form.submit({
        hostname: 'localhost',
        port,
        path: `/totp?_csrf=${csrfToken}`,
        method: 'POST',
        headers: {
          Accept: 'application/json',
          Cookie: cookie()
        }
      }, (err, res) => {
        expect(res.statusCode).to.equal(200);
        done();
      });
    });
  });

  it('should require oath code to reset passphrase', function(done) {
    getAndUpdateCsrfSessionAndToken('/settings', () => {
      let form = new FormData();
      form.append('passphrase_old', 'Keyboard Cat');
      form.append('passphrase_new', 'Keyboard Kitten');
      form.submit({
        hostname: 'localhost',
        port,
        path: `/passphrase?_csrf=${csrfToken}`,
        method: 'POST',
        headers: {
          Accept: 'application/json',
          Cookie: cookie()
        }
      }, (err, res) => {
        expect(res.statusCode).to.equal(401);
        done();
      });
    });
  });

  it('should reset the passphrase', function(done) {
    getAndUpdateCsrfSessionAndToken('/settings', () => {
      this.timeout(6000);
      let form = new FormData();
      form.append('passphrase_old', 'Keyboard Cat');
      form.append('passphrase_new', 'Keyboard Kitten');
      form.append('oath', totp.gen(oathSecret));
      form.submit({
        hostname: 'localhost',
        port,
        path: `/passphrase?_csrf=${csrfToken}`,
        method: 'POST',
        headers: {
          Accept: 'application/json',
          Cookie: cookie()
        }
      }, (err, res) => {
        expect(res.statusCode).to.equal(200);
        cookies = [];
        done();
      });
    });
  });

  it('should login and get the session token', function(done) {
    getAndUpdateCsrfSessionAndToken('/totp', () => {
      let form = new FormData();
      form.append('passphrase', 'Keyboard Kitten');
      form.append('oath', totp.gen(oathSecret));
      form.submit({
        hostname: 'localhost',
        port,
        path: `/login?_csrf=${csrfToken}`,
        method: 'POST',
        headers: {
          Accept: 'application/json',
          Cookie: cookie()
        }
      }, (err, res) => {
        expect(res.statusCode).to.equal(200);
        cookies = cookies.concat(res.headers['set-cookie']);
        done();
      });
    });
  });

  it('should require oath code to disable two factor', function(done) {
    getAndUpdateCsrfSessionAndToken('/settings', () => {
      let form = new FormData();
      form.submit({
        hostname: 'localhost',
        port,
        path: `/totp/delete?_csrf=${csrfToken}`,
        headers: {
          Accept: 'application/json',
          Cookie: cookie()
        }
      }, (err, res) => {
        expect(res.statusCode).to.equal(401);
        done();
      });
    });
  });

  it('should disable two factor', function(done) {
    getAndUpdateCsrfSessionAndToken('/settings', () => {
      let form = new FormData();
      form.append('oath', totp.gen(oathSecret));
      form.submit({
        hostname: 'localhost',
        port,
        path: `/totp/delete?_csrf=${csrfToken}`,
        headers: {
          Accept: 'application/json',
          Cookie: cookie()
        }
      }, (err, res) => {
        expect(res.statusCode).to.equal(200);
        done();
      });
    });
  });

  let file = crypto.randomBytes(3000);
  let key = null;
  let pointer = null;
  let mapping = null;

  it('should upload the object', function(done) {
    sinon.stub(node, 'upload').callsFake(function(m) {
      return new Promise((resolve) => {
        mapping = m;
        pointer = m.toBlobPointer();
        resolve(pointer);
      });
    });
    let form = new FormData();
    form.append('file', file, {
      filename: 'document.pdf',
      contentType: 'application/pdf',
      knownLength: 3000
    });
    getAndUpdateCsrfSessionAndToken('/objects', () => {
      form.submit({
        hostname: 'localhost',
        port,
        path: `/objects?_csrf=${csrfToken}`,
        method: 'POST',
        headers: {
          Accept: 'application/json',
          Cookie: cookie()
        }
      }, (err, res) => {
        expect(err).to.equal(null);
        expect(res.statusCode).to.equal(200);
        let body = '';
        res.on('data', (data) => body += data);
        res.on('end', () => {
          body = JSON.parse(body).object;
          key = body.key;
          expect(body.mimetype).to.equal('application/pdf');
          expect(body.filename).to.equal('document.pdf');
          expect(body.hashes).to.have.lengthOf(1);
          done();
        });
      });
    });
  });

  it('should return the object pointer list', function(done) {
    let req = http.request({
      hostname: 'localhost',
      port,
      path: '/objects',
      method: 'GET',
      headers: {
        Accept: 'application/json',
        Cookie: cookie()
      }
    }, (res) => {
      let body = '';
      res.on('data', data => body += data.toString());
      res.on('end', () => {
        body = JSON.parse(body).objects;
        expect(body).to.have.lengthOf(1);
        expect(body[0].mimetype).to.equal('application/pdf');
        expect(body[0].filename).to.equal('document.pdf');
        expect(body[0].hashes).to.have.lengthOf(1);
        done();
      });
    });
    req.end();
  });

  let href = null;

  it('should fetch the metadata for the object', function(done) {
    let req = http.request({
      hostname: 'localhost',
      port,
      path: '/objects/' + key + '/info',
      method: 'GET',
      headers: {
        Accept: 'application/json',
        Cookie: cookie()
      }
    }, (res) => {
      let body = '';
      res.on('data', data => body += data.toString());
      res.on('end', () => {
        body = JSON.parse(body).object;
        href = body.href;
        expect(body.mimetype).to.equal('application/pdf');
        expect(body.filename).to.equal('document.pdf');
        expect(body.hashes).to.have.lengthOf(1);
        done();
      });
    });
    req.end();
  });

  it('should forget the object pointer', function(done) {
    getAndUpdateCsrfSessionAndToken(`/objects/${key}/info`, () => {
      let req = http.request({
        hostname: 'localhost',
        port,
        path: `/objects/${key}/forget?_csrf=${csrfToken}`,
        method: 'POST',
        headers: {
          Accept: 'application/json',
          Cookie: cookie()
        }
      }, (res) => {
        expect(res.statusCode).to.equal(200);
        done();
      });
      req.end();
    });
  });

  it('should resolve the encrypted pointer and save it', function(done) {
    sinon.stub(node, 'resolve').callsFake(function() {
      return new Promise(resolve => {
        resolve(pointer);
      });
    });
    let body = '';
    let form = new FormData();
    form.append('href', href);
    getAndUpdateCsrfSessionAndToken('/objects', () => {
      form.submit({
        hostname: 'localhost',
        port,
        path: `/objects/resolve?_csrf=${csrfToken}`,
        method: 'POST',
        headers: {
          Accept: 'application/json',
          Cookie: cookie()
        }
      }, (err, res) => {
        res.on('data', (data) => body += data.toString());
        res.on('end', () => {
          body = JSON.parse(body).object;
          expect(body.mimetype).to.equal('application/pdf');
          expect(body.filename).to.equal('document.pdf');
          expect(body.hashes).to.have.lengthOf(1);
          done();
        });
      });
    });
  });

  it('should download the object', function(done) {
    sinon.stub(node, 'download').callsFake(function() {
      return new Promise(resolve => {
        resolve(mapping);
      });
    });
    let req = http.request({
      hostname: 'localhost',
      port,
      path: '/objects/' + key,
      method: 'GET',
      headers: {
        Accept: 'application/json',
        Cookie: cookie()
      }
    }, (res) => {
      expect(res.statusCode).to.equal(200);
      let body = '';
      res.on('data', data => body += data.toString('hex'));
      res.on('end', () => {
        expect(body).to.equal(file.toString('hex'));
        done();
      });
    });
    req.end();
  });

});
