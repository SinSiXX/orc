'use strict';

const { expect } = require('chai');
const constants = require('../lib/constants');
const Rules = require('../lib/rules');


describe('@class Rules', function() {

  describe('@method ensureUniformBlobSize', function() {

    it('should only pass if blob size is uniform', function(done) {
      const rules = new Rules();
      rules.ensureUniformBlobSize({
        params: [
          '0000000000000000000000000000000000000000',
          {
            value: Buffer.alloc(constants.UNIFORM_BLOB_SIZE).toString('base64')
          }
        ]
      }, {}, function(err) {
        expect(err).to.equal(undefined);
        done();
      });
    });

    it('should reject if blob size is not correct', function(done) {
      const rules = new Rules();
      rules.ensureUniformBlobSize({
        params: [
          '0000000000000000000000000000000000000000',
          {
            value: Buffer.alloc(4096).toString('base64')
          }
        ]
      }, {}, function(err) {
        expect(err.message).to.equal('Blob does not conform to required size');
        done();
      });
    });

  });

});
