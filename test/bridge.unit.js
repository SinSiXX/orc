'use strict';

const { expect } = require('chai');
const sinon = require('sinon');
const Bridge = require('../lib/bridge');
const bunyan = require('bunyan');


describe('@class Bridge', function() {

  describe('@method listen', function() {

    it('should start server', function(done) {
      let bridge = new Bridge({
        logger: bunyan.createLogger({ name: '-', level: 'fatal' })
      }, {});
      let listen = sinon.stub(bridge.server, 'listen');
      bridge.listen(0);
      setImmediate(() => {
        expect(listen.called).to.equal(true);
        done();
      });
    });

  });

});
