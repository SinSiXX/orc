  WELCOME TO ONION ROUTED CLOUD!

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  Checking Status:

    systemctl status orc

  Viewing Logs:

    journalctl -xeu orc

  Managing Service: 

    systemctl [start|stop|restart] orc

  Configuration:

    vim /etc/orc/config

  Re-Running Setup:

    /opt/deadcanaries/setup.sh

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  Access your ORC node's web interface via
  Tor Browser, by navigating to the hidden 
  service URL found in:

  /etc/orc/bridge.tor/hidden_service/hostname 
  
  (This file won't exist until your identity 
  is mined and your node is online)
