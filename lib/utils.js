/**
 * @module orc/utils
 */

'use strict';

const crypto = require('crypto');


/**
 * Splits a buffer into N buffers given a byte size
 * @param {buffer} buffer
 * @returns {buffer[]}
 */
module.exports.splitBufferByBytes = function(buffer, size) {
  let splitted = [];

  for (let begin = 0, end = size; ; begin = end, end += size) {
    if (begin === buffer.length) {
      break;
    }

    if (buffer.length < end) {
      splitted.push(buffer.slice(begin));
      break;
    }

    splitted.push(buffer.slice(begin, end));
  }

  return splitted;
};

/**
 * Returns a cipher stream using aes-256-cbc
 * @param {buffer} password
 * @param {buffer} salt
 * @param {buffer} iv
 * @returns {Cipher}
 */
module.exports.createCipher = function(password, salt, iv) {
  const key = crypto.scryptSync(password, salt, 32);
  const cipher = crypto.createCipheriv('aes-256-cbc', key, iv);

  return cipher;
};

/**
 * @param {buffer} data
 * @param {buffer} password
 * @param {buffer} salt
 * @param {buffer} iv
 * @returns {buffer}
 */
module.exports.encrypt = function(data, password, salt, iv) {
  const cipher = module.exports.createCipher(password, salt, iv);
  return Buffer.concat([cipher.update(data), cipher.final()]);
};

/**
 * Returns a decipher stream using aes-256-cbc
 * @param {buffer} password
 * @param {buffer} salt
 * @param {buffer} iv
 * @returns {Cipher}
 */
module.exports.createDecipher = function(password, salt, iv) {
  const key = crypto.scryptSync(password, salt, 32);
  const decipher = crypto.createDecipheriv('aes-256-cbc', key, iv);

  return decipher;
};

/**
 * @param {buffer} data
 * @param {buffer} password
 * @param {buffer} salt
 * @param {buffer} iv
 * @returns {buffer}
 */
module.exports.decrypt = function(data, password, salt, iv) {
  const decipher = module.exports.createDecipher(password, salt, iv);
  return Buffer.concat([decipher.update(data), decipher.final()]);
};
