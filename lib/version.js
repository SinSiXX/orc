/**
 * @module orc/version
 */

'use strict';

const os = require('os');


module.exports = {
  /**
   * @constant {string} protocol - The supported protocol version
   */
  protocol: '5.0.0',
  /**
   * @constant {string} software - The current software version
   */
  software: require('../package').version,
  /**
   * @constant {object} system - System platform, arch, release
   */
  system: {
    platform: os.platform(),
    arch: os.arch(),
    release: os.release()
  },
  /**
   * Returns human readable string of versions
   * @function
   * @returns {string}
   */
  toString: function() {
    let { software, protocol } = module.exports;
    return `orc v${software} protocol v${protocol}`;
  }
};
