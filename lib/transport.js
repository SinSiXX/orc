'use strict';

const merge = require('merge');
const { HTTPTransport } = require('@deadcanaries/kadence');
const { Agent } = require('http');


/**
 * Represents the ORC-specific HTTP transport
 */
class Transport extends HTTPTransport {

  /**
   * @constructor
   */
  constructor(options) {
    super(options);
  }

  /**
   * Make sure we explicity set the keepAlive options on requests
   * @private
   */
  _createRequest(options) {
    const request = super._createRequest(merge({
      agent: new Agent({ keepAlive: true, keepAliveMsecs: 25000 }),
      path: '/rpc/'
    }, options));
    request.setNoDelay(true);
    return request;
  }

  /**
   * Disable nagle algorithm on connections
   * @private
   */
  _createServer(options) {
    const server = super._createServer(options);
    server.on('connection', (sock) => sock.setNoDelay(true));
    return server;
  }

}

module.exports = Transport;
