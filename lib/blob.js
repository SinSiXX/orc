'use strict';

const constants = require('./constants');
const crypto = require('crypto');
const utils = require('./utils');
const kadence = require('@deadcanaries/kadence');
const { Duplex: DuplexStream } = require('stream');
const async = require('async');


/**
 * Provides a representation of the "seed" block for resolving a
 * {@link BlobMapping}. Can be derived from a mapping.
 */
class BlobPointer {

  /**
   * @constructor
   * @param {string} filename - Human readable filename with extension
   * @param {buffer[]} hashes - Ordered list of blob hashes
   * @param {BlobMapping~cryptparams} params - Encryption parameters
   */
  constructor(filename, hashes, params) {
    this.filename = filename;
    this.hashes = hashes;
    this.params = params;
  }

  /**
   * Returns a packed, encrypted, slice of this pointer
   * @returns {string[]}
   */
  toEncryptedSlice() {
    const data = JSON.stringify({
      filename: this.filename,
      hashes: this.hashes.map(h => h.toString('hex')),
      params: {
        password: this.params.password.toString('hex'),
        salt: this.params.salt.toString('hex'),
        iv: this.params.iv.toString('hex')
      }
    });
    const slice = Buffer.concat([
      Buffer.from([0x02, 0x00, 0x00, 0x00, 0x00]),
      Buffer.from(data),
      Buffer.alloc(constants.MAX_SLICE_SIZE - data.length - 5, 0)
    ]);
    slice.writeUInt32LE(Buffer.from(data).length + 5, 1);
    const encrypted = utils.encrypt(
      slice,
      this.params.password,
      this.params.salt,
      this.params.iv
    );
    const sliceInfo = {
      key: kadence.utils.hash160(encrypted).toString('hex'),
      value: encrypted.toString('base64')
    };
    sliceInfo.href = 'orc://' + Buffer.concat([
      Buffer.from(sliceInfo.key, 'hex'),
      this.params.password,
      this.params.salt,
      this.params.iv
    ]).toString('hex');

    return sliceInfo;
  }

  /**
   * Parses a slice link into a key and crypt params
   * @static
   * @param {string} href
   * @returns {BlobMapping~cryptparams}
   */
  static parseHref(href) {
    const [, infohash] = href.split('orc://');
    const buffer = Buffer.from(infohash, 'hex');
    const cryptparams = {
      password: buffer.slice(20, 52),
      salt: buffer.slice(52, 60),
      iv: buffer.slice(60, 76)
    };

    return [buffer.slice(0, 20).toString('hex'), cryptparams];
  }

}

/**
 * @typedef {object} BlobMapping~cryptparams
 * @property {buffer} password - N-length passphrase
 * @property {buffer} salt - 16 byte salt
 * @property {buffer} iv - 32 byte initialization vector
 */

/** @private */
function populateMissingCryptParams(params = {}) {
  if (!params.password) {
    params.password = crypto.randomBytes(32);
  }
  if (!params.salt) {
    params.salt = crypto.randomBytes(8);
  }
  if (!params.iv) {
    params.iv = crypto.randomBytes(16);
  }
  return params;
}

/**
 * Provides a representation of a blob mapping, which is a fully resolved
 * blob. Acts as an interface for constructing a mapping to distribute
 * through the network and derive a pointer.
 */
class BlobMapping extends DuplexStream {

  /**
   * @constructor
   * @param {string} filename - Human readable file name with extension
   * @param {BlobMapping~cryptparams} [params] - Encryption parameters
   */
  constructor(filename, params = { password: null, salt: null, iv: null }) {
    super();
    populateMissingCryptParams(params);

    this.filename = filename;
    this.slices = new Map();
    this.params = params;

    this._reinitializeBuffer();
  }

  /** @private */
  _write(chunk, encoding, callback) {
    const bufferSize = this._buffer.length;
    const availableInBuffer = constants.MAX_SLICE_SIZE - bufferSize;
    const bufferCanHoldChunk = chunk.length <= availableInBuffer;

    if (bufferCanHoldChunk) {
      this._buffer = Buffer.concat([this._buffer, chunk]);
    } else {
      this._finalizeSlice(availableInBuffer);
      this._reinitializeBuffer();

      if (chunk.length > (constants.MAX_SLICE_SIZE - 5)) {
        const parts = utils.splitBufferByBytes(chunk,
          constants.MAX_SLICE_SIZE - 5);

        for (let part of parts) {
          this._buffer = Buffer.concat([this._buffer, part]);
          this._finalizeSlice(constants.MAX_SLICE_SIZE - this._buffer.length);
          this._reinitializeBuffer();
        }
      } else {
        this._buffer = Buffer.concat([this._buffer, chunk]);
      }
    }

    callback();
  }

  /** @private */
  _read() {
    async.until(this._pushNextSliceIfAvailable.bind(this), (done) => {
      setTimeout(done, 500);
    });
  }

  /** @private */
  _pushNextSliceIfAvailable() {
    if (this.slices.size === 0) {
      return this.push(null);
    }

    let [hash, buffer] = [...this.slices.entries()].shift();

    if (buffer.length === constants.UNIFORM_BLOB_SIZE) {
      try {
        buffer = utils.decrypt(buffer, this.params.password,
          this.params.salt, this.params.iv);
      } catch (err) {
        return this.emit('error', err);
      }

      const type = buffer[0];

      if (type === 0x01) { // This is a raw blob slice
        const end = buffer.readUInt32LE(1);
        const cleartext = buffer.slice(5, end);

        this.slices.delete(hash);
        this.push(cleartext);
        return true;
      } else {
        // NB: Reserved for future protocol enhancements
        return this.emit('error', new Error(`Unknown slice type "${type}"`));
      }
    }

    return false;
  }

  /** @private */
  _final(callback) {
    this._finalizeSlice(constants.MAX_SLICE_SIZE - this._buffer.length);
    this._reinitializeBuffer();
    callback();
  }

  /** @private */
  _finalizeSlice(padding) {
    this._buffer.writeUInt32LE(
      constants.MAX_SLICE_SIZE - padding,
      1
    );

    this._buffer = utils.encrypt(
      Buffer.concat([this._buffer, Buffer.alloc(padding)]),
      this.params.password,
      this.params.salt,
      this.params.iv
    );

    this.slices.set(kadence.utils.hash160(this._buffer).toString('hex'),
      Buffer.from(this._buffer));
  }

  /** @private */
  _reinitializeBuffer() {
    this._buffer = Buffer.from([0x01, 0x00, 0x00, 0x00, 0x00]);
  }

  /**
   * Accepts an ordered list of hashes to prepopulate the slice map
   * @param {BlobPointer} pointer
   */
  static fromBlobPointer(pointer) {
    const mapping = new BlobMapping(pointer.filename, pointer.params);

    for (let hash of pointer.hashes) {
      mapping.slices.set(hash.toString('hex'), Buffer.from([]));
    }

    return mapping;
  }

  /**
   * Writes the given buffer to the appropriate entry in the slice map
   * @param {buffer} slice
   * @returns {Map|null}
   */
  writeToSliceMap(slice) {
    const hash = kadence.utils.hash160(slice).toString('hex');

    if (!this.slices.has(hash)) {
      return false;
    }

    return this.slices.set(hash, slice);
  }

  /**
   * Aborts slice map construction and empties the slice map
   */
  abort() {
    this.slices.clear();
    this.emit('abort');
  }

  /**
   * Creates a {@link BlobPointer} from the full mapping
   * @returns {BlobPointer}
   */
  toBlobPointer() {
    return new BlobPointer(
      this.filename,
      [...this.slices.keys()].map(hash => Buffer.from(hash, 'hex')),
      this.params
    );
  }

}

module.exports.BlobMapping = BlobMapping;
module.exports.BlobPointer = BlobPointer;
