/**
 * @module orc/constants
 */

'use strict';

/**
 * @constant {number} UNIFORM_BLOB_SIZE - Required blob size for entries
 */
module.exports.UNIFORM_BLOB_SIZE = 2097152;

/**
 * @constant {number} MAX_SLICE_SIZE
 */
module.exports.MAX_SLICE_SIZE = 2097148;
