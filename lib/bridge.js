'use strict';

/* eslint max-statements: [2, 30] */

const async = require('async');
const { EventEmitter } = require('events');
const BusBoy = require('busboy');
const http = require('http');
const serveStatic = require('serve-static');
const cookieParser = require('cookie-parser');
const utils = require('./utils');
const fs = require('fs');
const merge = require('merge');
const express = require('express');
const crypto = require('crypto');
const { tmpdir, EOL } = require('os');
const path = require('path');
const uuid = require('uuid');
const ms = require('ms');
const cors = require('cors');
const Secret = require('./secret');
const base32 = require('thirty-two');
const notp = require('notp');
const qr = require('qr-image');
const kadence = require('@deadcanaries/kadence');
const { BlobPointer, BlobMapping } = require('./blob');
const mime = require('mime-types');
const keytar = require('keytar');
const csurf = require('csurf');


/**
 * Bridge exposes a simple API for administering an ORC node, uploading,
 * and downloading objects.
 */
class Bridge extends EventEmitter {

  static get DEFAULTS() {
    return {
      privateKeyPath: null,
      cryptParams: {
        salt: null,
        iv: null
      },
      logFilePath: null,
      otpSecretFilePath: path.join(tmpdir(), 'otpsecret'),
      storage: null,
      passphraseFile: null
    };
  }

  /**
   * @constructor
   * @param {Node} node
   * @param {object} options
   * @param {string} options.privateKeyPath - Path the the node's private key
   * @param {object} options.cryptParams - Crypto params
   * @param {buffer} options.cryptParams.salt
   * @param {buffer} options.cryptParams.iv
   * @param {string} options.logFilePath - Path to read logs from
   * @param {string} options.otpSecretFilePath - Path to file where 2FA secret is kept
   * @param {object} options.storage - LevelUp compatible storage adapter
   * @param {Secret} options.secret - Initialized secret for decryption
   * @param {string} [options.passphraseFile] - Sync passphrase reset with file
   * @param {boolean} [options.syncPassphraseToKeychain=false]
   */
  constructor(node, options) {
    super();

    this._started = Date.now();

    this.options = merge(Bridge.DEFAULTS, options);
    this.api = express();
    this.node = node;
    this.server = this._createServer(this.api);
    this.sessions = new Map();
    this.timers = new Map();
    this.secret = this.options.secret;
    this.storage = this.options.storage;

    this.server.setTimeout(0);
    this._bindRoutes();
  }

  /**
   * @private
   */
  _createServer(handler) {
    return http.createServer(handler);
  }

  /**
   * Listens on the given port and hostname
   * @param {number} port
   * @param {string} hostname
   * @param {function} callback
   */
  listen() {
    this.server.listen(...arguments);
  }

  /**
   * Creates request router and handler stack
   * @private
   * @returns {function}
   */
  _bindRoutes() {
    const auth = this.authenticate.bind(this);

    // Setup template engine for GUI
    this.api.set('views', path.join(__dirname, '../views'));
    this.api.set('view engine', 'pug');

    // Used for all routes
    this.api.disable('x-powered-by');
    this.api.use(cors());
    this.api.use(cookieParser());
    this.api.use(csurf({ cookie: true }));

    // Serve up static assets for the web interface
    this.api.use(serveStatic(path.join(__dirname, '../static')));

    // Generate stats endpoint
    this.api.get('/', auth, this.getNodeStatus.bind(this));

    // Used for peer profiles
    this.api.get('/peers', auth, this.listProfiles.bind(this));
    this.api.get('/peers/:identity', auth, this.getProfile.bind(this));

    // Used for manipulating objects
    this.api.get('/objects', auth, this.listObjects.bind(this));
    this.api.get('/objects/:id', auth, this.downloadObject.bind(this));
    this.api.get('/objects/:id/info', auth, this.getObjectInfo.bind(this));
    this.api.post('/objects', auth, this.uploadObject.bind(this));
    this.api.post('/objects/resolve', auth, this.resolveObject.bind(this));
    this.api.post('/objects/:id/forget', auth, this.forgetObject.bind(this));

    // Used for authentication
    this.api.post('/login', this.login.bind(this));
    this.api.get('/logout', auth, this.logout.bind(this));
    this.api.post('/passphrase', auth, this.changePassphrase.bind(this));
    this.api.get('/totp', auth, this.generateTotpSecret.bind(this));
    this.api.post('/totp', auth, this.setupTotpSecret.bind(this));
    this.api.get('/totp/qr', auth, this.getTotpQrCode.bind(this));
    this.api.post('/totp/delete', auth, this.disableTotpSecret.bind(this));

    // Settings and debugging
    this.api.get('/settings', auth, this.getBridgeSettings.bind(this));
    this.api.get('/messages', auth, this.getMessageLogs.bind(this));

    // Handle everything else with a custom 404
    this.api.get('*', this.notFoundFallthrough.bind(this));

    // Fallthrough to error handler
    this.api.use(this.error.bind(this));
  }

  /**
   * Handles request authentication if defined
   */
  authenticate(req, res, next) {
    const prv = fs.readFileSync(this.options.privateKeyPath);
    const error = new Error('Not authorized');
    error.code = 401;

    if (prv.length === 32) {
      return next(new Error(
        'The web interface has been temporarily disabled. ' +
          'To re-enable it, the node operator must set a passphrase to ' +
          'encrypt the node\'s private key'
      ));
    }

    const locals = {
      meta: this._getMetaData(req),
    };

    if (!req.cookies.token || !this.sessions.has(req.cookies.token)) {
      return res.format({
        html: () => res.render('login', locals),
        json: () => next(error)
      });
    }

    this._resetSessionTimeout(req.cookies.token);
    next();
  }

  /**
   * Invalidates the authentication token supplied
   */
  logout(req, res) {
    this.sessions.delete(req.cookies.token);
    clearTimeout(this.timers.get(req.cookies.token));
    this.timers.delete(req.cookies.token);
    res.clearCookie('token', { httpOnly: true });
    res.format({
      html: () => res.redirect('/'),
      json: () => res.json({})
    });
  }

  /**
   * Checks the passphrase against the local salted hash and sets a session
   * cookie
   */
  login(req, res, next) {
    const busboy = new BusBoy({ headers: req.headers });
    let passphrase, oath, keepalive = ms('24h');

    busboy.on('field', (name, value) => {
      if (name === 'passphrase') {
        passphrase = value;
      } else if (name === 'oath') {
        oath = value;
      } else if (name === 'keepalive' && ms(value)) {
        keepalive = ms(value);
      }
    });

    busboy.on('finish', () => {
      if (!this._checkPassphrase(passphrase) || !this._checkOathToken(oath)) {
        const err = new Error('Not authorized');
        err.code = 401;
        return next(err);
      }

      const token = uuid.v4();

      this.sessions.set(token, keepalive);
      res.cookie('token', token, { maxAge: keepalive, httpOnly: true });
      this._resetSessionTimeout(token);

      res.format({
        html: () => res.redirect('/'),
        json: () => res.json({})
      });
    });

    busboy.on('error', next);
    req.pipe(busboy);
  }

  /**
   * @private
   */
  _checkOathToken(token, secretpath) {
    let secret = null;
    secretpath = secretpath || this.options.otpSecretFilePath;

    if (!fs.existsSync(secretpath)) {
      return true;
    }

    if (!token) {
      return false;
    }

    try {
      secret = utils.decrypt(
        fs.readFileSync(secretpath),
        this.node.spartacus.privateKey,
        this.options.cryptParams.salt,
        this.options.cryptParams.iv
      );
      token = token.replace(/\W+/g, '');
    } catch (err) {
      return false;
    }

    return notp.totp.verify(token, secret, { window: 4, time: 30 });
  }

  /**
   * @private
   */
  _resetSessionTimeout(token) {
    clearTimeout(this.timers.get(token));
    const timeout = setTimeout(() => {
      this.sessions.delete(token);
      this.timers.delete(token);
    }, this.sessions.get(token));
    this.timers.set(token, timeout);
  }

  /**
   * @private
   */
  _checkPassphrase(passphrase) {
    let cleartext;

    try {
      const secret = new Secret(passphrase, this.options.cryptParams.salt);
      const prv = fs.readFileSync(this.options.privateKeyPath);
      cleartext = secret.decrypt(prv);
    } catch (err) {
      return false;
    }

    return cleartext.length === 32;
  }

  /**
   * Changes the authentication passphrase
   */
  changePassphrase(req, res, next) {
    const busboy = new BusBoy({ headers: req.headers });
    let passphraseNew, passphraseOld, totpToken;

    busboy.on('field', (name, value) => {
      if (name === 'passphrase_new') {
        passphraseNew = value;
      }

      if (name === 'passphrase_old') {
        passphraseOld = value;
      }

      if (name === 'oath') {
        totpToken = value;
      }
    });

    busboy.on('finish', () => {
      if (!this._checkPassphrase(passphraseOld)) {
        const err = new Error('Invalid passphrase');
        err.code = 401;
        return next(err);
      }

      if (!this._checkOathToken(totpToken)) {
        const err = new Error('Invalid TOTP code');
        err.code = 401;
        return next(err);
      }

      let prv = fs.readFileSync(this.options.privateKeyPath);
      let oldSecret, newSecret;

      try {
        oldSecret = new Secret(passphraseOld, this.options.cryptParams.salt);
        prv = oldSecret.decrypt(prv);
        newSecret = new Secret(passphraseNew, this.options.cryptParams.salt);
        prv = newSecret.encrypt(prv);
      } catch (err) {
        return next(new Error('Invalid passphrase'));
      }

      fs.writeFileSync(this.options.privateKeyPath, prv);

      if (this.options.syncPassphraseToKeychain) {
        try {
          keytar.setPassword('ORC', this.options.privateKeyPath, passphraseNew);
        } catch (err) {
          this.node.logger.error(err.message);
        }
      }

      // If the process was started non-interactively via a passphrase file
      // we want to make sure we update that file, too.
      if (this.options.passphraseFile) {
        fs.writeFileSync(this.options.passphraseFile, passphraseNew);
      }

      this.sessions.clear();
      res.format({
        html: () => res.redirect('/'),
        json: () => res.json({})
      });
    });

    busboy.on('error', next);
    req.pipe(busboy);
  }

  /**
   * Responds to requests with error code and message
   */
  error(err, req, res, next) {
    if (!err) {
      return next();
    }

    if (!err.code || !Number.isInteger(err.code) || err.code > 500) {
      err.code = 500;
    }

    if (err.notFound) {
      err.code = 404;
    }

    const meta = this._getMetaData(req);
    const locals = {
      meta: {
        csrfToken: meta.csrfToken
      },
      error: err.message,
      code: err.code || 500,
    };

    res.status(Number.isInteger(err.code) ? err.code : 500).format({
      html: () => res.render('error', locals),
      json: () => res.json(locals)
    });
  }

  /**
   * @private
   */
  notFoundFallthrough(req, res, next) {
    const error = new Error(`Endpoint "${req.path}" does not exist`);
    error.code = 404;
    next(error);
  }

  /**
   * Get metadata for node to include in responses
   * @private
   */
  _getMetaData(req, category) {
    return {
      identity: this.node.identity.toString('hex'),
      contact: this.node.contact,
      versions: require('./version'),
      uptime: ms(Date.now() - this._started, { long: true }),
      category,
      totp: fs.existsSync(this.options.otpSecretFilePath),
      onion: this.options.onionHostname // TODO: Build this into the bridge
        ? this.options.onionHostname()
        : null,
      csrfToken: req.csrfToken()
    };
  }

  /**
   * Returns status information about the running node
   */
  getNodeStatus(req, res) {
    let peers = [];

    this.node.router.forEach(bucket => {
      for (let contact of bucket) {
        if (this.node.router.size > 1) {
          if (contact[0] === '0000000000000000000000000000000000000000') {
            continue;
          }
        }
        peers.push(contact);
      }
    });

    const locals = {
      meta: this._getMetaData(req, 'status'),
      peers,
    };

    res.format({
      html: () => res.render('node-status', locals),
      json: () => res.json(locals)
    });
  }

  /**
   * Scans the object database and returns all index entries
   */
  listObjects(req, res, next) {
    const locals = {
      meta: this._getMetaData(req, 'files'),
      objects: [],
    };

    this.storage.createValueStream({ valueEncoding: 'json' })
      .on('data', (data) => locals.objects.push(data))
      .on('end', () => {
        res.format({
          html: () => res.render('object-list', locals),
          json: () => res.json(locals)
        });
      })
      .on('error', next);
  }

  /**
   * Gets object information by unique ID
   */
  getObjectInfo(req, res, next) {
    const locals = {
      meta: this._getMetaData(req, 'files')
    };

    this.storage.get(req.params.id, { valueEncoding: 'json' }, (err, value) => {
      if (err) {
        return next(err);
      }

      locals.object = value;

      res.format({
        html: () => res.render('object-detail', locals),
        json: () => res.json(locals)
      });
    });
  }

  /**
   * Creates a blob mapping, uploads it to the network, saves an entry
   * into the local database, returns the object details
   */
  uploadObject(req, res, next) {
    const busboy = new BusBoy({ headers: req.headers });

    busboy.on('file', (field, file, filename) => {
      const mapping = new BlobMapping(filename);

      mapping.on('finish', () => {
        this.node.upload(mapping).then((pointer) => {
          const { key, href } = pointer.toEncryptedSlice();
          const object = {
            filename: pointer.filename,
            hashes: pointer.hashes.map(h => h.toString('hex')),
            key, href,
            mimetype: mime.lookup(pointer.filename)
          };

          this.storage.put(key, object, { valueEncoding: 'json' }, (err) => {
            if (err) {
              return next(err);
            }

            const locals = {
              meta: this._getMetaData(req, 'files'),
              object
            };

            res.format({
              html: () => res.redirect(`/objects/${object.key}/info`),
              json: () => res.json(locals)
            });
          });
        }, next);
      });

      file.pipe(mapping);
    });

    busboy.on('error', next);
    req.pipe(busboy);
  }

  /**
   * Downloads the object from the network
   */
  downloadObject(req, res, next) {
    this.storage.get(req.params.id, { valueEncoding: 'json' }, (err, obj) => {
      if (err) {
        return next(err);
      }

      const [, cryptparams] = BlobPointer.parseHref(obj.href);
      const pointer = new BlobPointer(
        obj.filename,
        obj.hashes.map(h => Buffer.from(h, 'hex')),
        cryptparams
      );

      this.node.download(pointer).then((mapping) => {
        res.writeHead(200, {
          'Content-Type': obj.mimetype,
          'Content-Disposition': `attachment; filename="${mapping.filename}"`,
          'Transfer-Encoding': 'chunked'
        });
        mapping.pipe(res);
      }, next);
    });
  }

  /**
   * Drops the local pointer reference
   */
  forgetObject(req, res, next) {
    this.storage.del(req.params.id, (err) => {
      if (err) {
        return next(err);
      }

      const locals = {
        meta: this._getMetaData(req, 'files')
      };

      res.format({
        html: () => res.redirect('/objects'),
        json: () => res.json(locals)
      });
    });
  }

  /**
   * Accepts a body containing a magnet link, resolves the pointer and creates
   * a local object pointer record, then returns it. Clients can follow with a
   * GET /objects/:id to download the object
   */
  resolveObject(req, res, next) {
    const busboy = new BusBoy({ headers: req.headers });
    let href = null;

    busboy.once('field', (fieldname, val) => {
      if (fieldname === 'href') {
        href = val;
      }
    });

    busboy.on('finish', () => {
      if (!href) {
        const err = new Error('Bad request');
        err.code = 400;
        return next(err);
      }

      this.node.resolve(href).then((pointer) => {
        const { key, href } = pointer.toEncryptedSlice();
        const object = {
          filename: pointer.filename,
          hashes: pointer.hashes.map(h => h.toString('hex')),
          key, href,
          mimetype: mime.lookup(pointer.filename)
        };

        this.storage.put(key, object, { valueEncoding: 'json' }, (err) => {
          if (err) {
            return next(err);
          }

          const locals = {
            meta: this._getMetaData(req, 'files'),
            object
          }

          res.format({
            html: () => res.redirect(`/objects/${key}/info`),
            json: () => res.json(locals)
          });
        });
      }, next);
    });

    busboy.on('error', next);
    req.on('error', next).pipe(busboy);
  }

  /**
   * Returns all recorded peers in the cache
   */
  listProfiles(req, res, next) {
    this.node.peers.getBootstrapCandidates().then((urls) => {
      async.map(urls, (url, next) => {
        const [identity] = kadence.utils.parseContactURL(url);
        this.node.peers.getExternalPeerInfo(identity).then(profile => {
          profile.identity = identity;
          next(null, profile);
        }, next);
      }, (err, profiles) => {
        if (err) {
          return next(err);
        }

        // Remove bootstrap seeds from the response
        profiles = profiles.filter((p) => {
          return p.identity !== '0000000000000000000000000000000000000000';
        });

        const locals = {
          meta: this._getMetaData(req, 'directory'),
          profiles
        };

        res.format({
          html: () => res.render('peer-list', locals),
          json: () => res.json(locals)
        });
      });
    }, next);
  }

  /**
   * Returns the profile given the peer fingerprint
   */
  getProfile(req, res, next) {
    this.node.peers.getExternalPeerInfo(
      req.params.identity
    ).then((profile) => {
      profile.identity = req.params.identity;

      const locals = {
        meta: this._getMetaData(req, 'directory'),
        profile
      };

      res.format({
        html: () => res.render('peer-detail', locals),
        json: () => res.json(locals)
      });
    }, (err) => {
      err = new Error(`Peer profile not found: ${err.message}`);
      err.code = 404;
      next(err);
    });
  }

  /**
   * Returns message logs
   */
  /* istanbul ignore next */
  getMessageLogs(req, res) {
    let logFile = '';

    try {
      logFile = fs.readFileSync(this.options.logFilePath).toString();
    } catch (err) {
      this.node.logger.error(`failed to open log file: ${err.message}`);
    }

    logFile = logFile.split(EOL);

    if (logFile.length > 200) {
      logFile = logFile.slice(logFile.length - 200 + 1);
    }

    const logs = logFile.map(l => {
      try {
        return JSON.parse(l);
      } catch (err) {
        return null;
      }
    }).reverse().filter(l => l !== null);

    const locals = {
      meta: this._getMetaData(req, 'messages'),
      logs
    };

    res.format({
      html: () => res.render('node-logs', locals),
      json: () => res.json(locals)
    })
  }

  /**
   * Returns this bridge's settings
   */
  getBridgeSettings(req, res) {
    const locals = {
      meta: this._getMetaData(req, 'settings')
    };

    res.format({
      html: () => res.render('node-admin', locals),
      json: () => res.json(locals)
    })
  }

  /**
   * Sets up TOTP secret
   */
  setupTotpSecret(req, res, next) {
    let oath;

    const otpfinal = this.options.otpSecretFilePath;
    const otptemp = `${otpfinal}.tmp`;

    if (fs.existsSync(otpfinal)) {
      return next(new Error('Two factor authentication is already setup'));
    }

    if (!fs.existsSync(otptemp)) {
      return next(new Error('TOTP secret has not been generated'));
    }

    const busboy = new BusBoy({ headers: req.headers });

    busboy.on('field', (name, value) => {
      if (name === 'oath') {
        oath = value;
      }
    });

    busboy.on('finish', () => {
      if (!this._checkOathToken(oath, otptemp)) {
        const err = new Error('Invalid TOTP code');
        err.code = 401;
        return next(err);
      }

      try {
        fs.renameSync(otptemp, otpfinal);
      } catch (err) {
        return next(new Error('Failed to setup 2FA'));
      }

      const locals = {
        meta: this._getMetaData(req, 'settings')
      };

      res.format({
        html: () => res.redirect('/settings'),
        json: () => res.json(locals)
      })
    });

    busboy.on('error', next);
    req.pipe(busboy);
  }

  /**
   * Generates a TOTP secret code
   */
  generateTotpSecret(req, res, next) {
    const secret = crypto.randomBytes(20);
    const identity = this.node.identity.toString('hex');

    try {
      fs.writeFileSync(
        `${this.options.otpSecretFilePath}.tmp`,
        utils.encrypt(
          secret,
          this.node.spartacus.privateKey,
          this.options.cryptParams.salt,
          this.options.cryptParams.iv
        )
      );
    } catch (err) {
      return next(new Error('Failed to generate TOTP secret'));
    }

    const secretStr = base32.encode(secret).toString('utf8')
      .replace(/=/g, '')
      .toLowerCase()
      .replace(/(\w{4})/g, '$1 ')
      .trim()
      .split(' ')
      .join('')
      .toUpperCase();

    const locals = {
      meta: this._getMetaData(req, 'settings'),
      totp: {
        secret: secretStr,
        href: `otpauth://totp/ORC:${identity}?secret=${secretStr}`
      }
    };

    res.format({
      html: () => res.render('node-2fa', locals),
      json: () => res.json(locals)
    });
  }

  /**
   * @private
   */
  getTotpQrCode(req, res, next) {
    let secret, identity = this.node.identity.toString('hex');

    if (!fs.existsSync(`${this.options.otpSecretFilePath}.tmp`)) {
      const err = new Error('Not found');
      err.code = 404;
      return next(err);
    }

    try {
      secret = utils.decrypt(
        fs.readFileSync(`${this.options.otpSecretFilePath}.tmp`),
        this.node.spartacus.privateKey,
        this.options.cryptParams.salt,
        this.options.cryptParams.iv
      );
      secret = base32.encode(secret).toString('utf8')
        .replace(/=/g, '')
        .toLowerCase()
        .replace(/(\w{4})/g, '$1 ')
        .trim()
        .split(' ')
        .join('')
        .toUpperCase();
    } catch (err) {
      return next(err);
    }

    qr.image(`otpauth://totp/ORC:${identity}?secret=${secret}`).pipe(res);
  }

  /**
   * Disables TOTP two-factor-authentication
   */
  disableTotpSecret(req, res, next) {
    const busboy = new BusBoy({ headers: req.headers });
    let oath;

    if (!fs.existsSync(this.options.otpSecretFilePath)) {
      const err = new Error('2FA is not enabled');
      err.code = 400;
      return next(err);
    }

    busboy.on('field', (name, value) => {
      if (name === 'oath') {
        oath = value;
      }
    });

    busboy.on('finish', () => {
      if (!this._checkOathToken(oath)) {
        const err = new Error('Invalid TOTP token');
        err.code = 401;
        return next(err);
      }

      try {
        fs.unlinkSync(this.options.otpSecretFilePath);
      } catch (err) {
        return next(err);
      }

      const locals = {
        meta: this._getMetaData(req, 'settings')
      };

      res.format({
        html: () => res.redirect('/settings'),
        json: () => res.json(locals)
      });
    });

    busboy.on('error', next);
    req.pipe(busboy);
  }

}

module.exports = Bridge;
