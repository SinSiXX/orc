'use strict';

const { tmpdir } = require('os');
const { join } = require('path');
const { randomBytes } = require('crypto');
const async = require('async');
const { createLogger } = require('bunyan');
const merge = require('merge');
const kadence = require('@deadcanaries/kadence');
const { BlobPointer, BlobMapping } = require('./blob');
const utils = require('./utils');
const Rules = require('./rules');
const Transport = require('./transport');


/**
 * Extends Kademlia with ORC protocol rules
 * @license AGPL-3.0
 */
class Node extends kadence.KademliaNode {

  static get DEFAULTS() {
    return {
      logger: createLogger({ name: 'orc' }),
      transport: new Transport(),
      privateKey: null,
      hashcashDifficulty: 4,
      peerCachePath: join(tmpdir(), 'peers-' + randomBytes(6).toString('hex'))
    };
  }

  /**
   * @constructor
   * @extends {KademliaNode}
   * @param {object} options
   * @param {buffer} options.privateKey - SECP256k1 private key
   * @param {kadence.eclipse.EclipseIdentity} identity - Solved identity
   */
  constructor(options) {
    /* eslint max-statements: [2, 16] */
    const opts = merge(Node.DEFAULTS, options, {
      identity: options.identity.fingerprint
    });

    super(opts);

    // Require a modest proof of work for every STORE RPC
    this.hashcash = this.plugin(kadence.hashcash({
      methods: ['STORE'],
      difficulty: opts.hashcashDifficulty
    }));

    // Sign and verify all RPC messages
    this.spartacus = this.plugin(kadence.spartacus(
      options.privateKey,
      { checkPublicKeyHash: false }
    ));

    // Force all network blobs to be keyed by their hash
    this.content = this.plugin(kadence.contentaddress({
      valueEncoding: 'base64'
    }));

    // Require equihash proof of work for all node identities to mitigate
    // eclipse and sybil attacks. This is not perfect and is an area of
    // active research.
    this.eclipse = this.plugin(kadence.eclipse(options.identity));

    // Keep track of peers we have seen and store them in a structured file
    this.peers = this.plugin(kadence.rolodex(options.peerCachePath));

    // Apply all of our additional middleware rules and RPC handlers
    const handlers = new Rules(this);
    this.use('STORE', handlers.ensureUniformBlobSize.bind(handlers));
  }

  /**
   * Accepts an ORC blob mapping and uses it to store the shards and pointers in
   * the network.
   * @param {BlobMapping} blobMapping
   * @returns {Promise<BlobPointer>}
   */
  upload(blobMapping) {
    return new Promise((resolve, reject) => {
      const pointer = blobMapping.toBlobPointer();

      const slices = [...blobMapping.slices.entries()];
      const limit = kadence.constants.ALPHA;

      async.eachLimit(slices, limit, ([hash, buffer], next) => {
        const encoded = buffer.toString('base64');

        async.retry(3, done => {
          this.iterativeStore(hash, encoded, (err, stored) => {
            if (err || stored === 0) {
              return done(err || new Error('Failed to store slice'));
            }

            done();
          });
        }, next);
      }, (err) => {
        if (err) {
          return reject(err);
        }

        const { key, value } = pointer.toEncryptedSlice();

        this.iterativeStore(key, value, (err, stored) => {
          if (err || stored === 0) {
            return reject(err || new Error('Failed to store pointer'));
          }

          resolve(pointer);
        })
      });
    });
  }

  /**
   * Accepts an ORC blob pointer and resolves all the parts, assembling them,
   * decrypting them, and returning the BlobMapping.
   * @param {BlobPointer} blobPointer
   * @returns {Promise<BlobMapping>}
   */
  download(blobPointer) {
    return new Promise((resolve, reject) => {
      const mapping = BlobMapping.fromBlobPointer(blobPointer);

      const hashes = blobPointer.hashes;
      const limit = kadence.constants.ALPHA;

      async.eachLimit(hashes, limit, (hash, next) => {
        const encoded = hash.toString('hex');

        async.retry(3, done => {
          this.iterativeFindValue(encoded, (err, result) => {
            if (err || Array.isArray(result)) { // List of contacts if no value
              return done(err || new Error('Failed to find slice'));
            }

            mapping.writeToSliceMap(Buffer.from(result.value, 'base64'));
            done();
            resolve(mapping); // Resolve mapping as soon as data is available
          });
        }, next);
      }, (err) => {
        if (err) {
          reject(err);
          mapping.abort();
        }
      });
    });
  }

  /**
   * Accepts a pointer info link and resolves the encrypted slice, decrypts it,
   * and returns a {@link BlobPointer}.
   * @param {string} href
   * @returns {Promise<BlobPointer>}
   */
  resolve(href) {
    return new Promise((resolve, reject) => {
      const [key, { password, salt, iv }] = BlobPointer.parseHref(href);

      async.retry(3, done => {
        this.iterativeFindValue(key, (err, result) => {
          if (err || Array.isArray(result)) {
            return done(err || new Error('Failed to find slice'));
          }

          done(null, result);
        });
      }, (err, result) => {
        if (err) {
          return reject(err);
        }

        let cleartext, json;

        try {
          cleartext = utils.decrypt(
            Buffer.from(result.value, 'base64'),
            password,
            salt,
            iv
          );
        } catch (err) {
          return reject(err);
        }

        if (cleartext[0] !== 0x02) { // Type param that says this is a pointer
          return reject(new Error('Slice is not a pointer'));
        }

        json = cleartext.slice(5, cleartext.readUInt32LE(1));

        try {
          json = JSON.parse(json);
        } catch (err) {
          return reject(new Error('Pointer slice is not valid JSON'));
        }

        resolve(new BlobPointer(
          json.filename,
          json.hashes.map(h => Buffer.from(h, 'hex')),
          {
            password: Buffer.from(json.params.password, 'hex'),
            salt: Buffer.from(json.params.salt, 'hex'),
            iv: Buffer.from(json.params.iv, 'hex')
          }
        ));
      });
    });
  }

}

module.exports = Node;
