'use strict';

const constants = require('./constants');


/**
 * Represents ORC protocol handlers
 */
class Rules {

  /**
   * @constructor
   * @param {Node} node
   */
  constructor(node) {
    this.node = node;
  }

  /**
   * Ensures that a network blob is uniform is size to all others. This is
   * to maintain privacy so that all blobs looks the same and cannot be
   * correlated to other blobs.
   * @param {kadence~Request} request
   * @param {kadence~Response} response
   * @param {function} next
   */
  ensureUniformBlobSize(request, response, next) {
    const [, item] = request.params;
    const blob = Buffer.from(item.value, 'base64');

    if (blob.length !== constants.UNIFORM_BLOB_SIZE) {
      return next(new Error('Blob does not conform to required size'));
    }

    next();
  }

}

module.exports = Rules;
