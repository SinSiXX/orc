'use strict';

const assert = require('assert');
const crypto = require('crypto');
const read = require('read');


/**
 * Given a passphrase, encrypts the target data and returns it
 * @memberof Secret
 * @instance
 * @param {buffer|string} target
 * @returns {buffer}
 */
function encrypt(key, target) {
  const cipher = crypto.createCipher('aes-256-cbc', key);
  return Buffer.concat([cipher.update(target), cipher.final()]);
}

/**
 * Given a passphrase, decrypts the target data and returns it
 * @memberof Secret
 * @instance
 * @param {buffer|string} target
 * @returns {buffer}
 */
function decrypt(key, target) {
  const decipher = crypto.createDecipher('aes-256-cbc', key);
  return Buffer.concat([decipher.update(target), decipher.final()]);
}

/**
 * Returns the derived key from the passphrase
 * @memberof Secret
 * @instance
 * @private
 * @param {buffer|string} passphrase
 * @returns {buffer}
 */
function key(passphrase, salt) {
  return crypto.pbkdf2Sync(passphrase, salt, 100000, 64, 'sha512');
}

/**
 * Represents a secret key/passphrase and provides utilities for
 * encryption and decryption of other arbitrary secrets
 */
class Secret {

  static request(prompt, salt, passphrase, onReadResult = () => null) {
    return new Promise((resolve, reject) => {
      if (passphrase) {
        try {
          return resolve(new Secret(passphrase, salt));
        } catch (err) {
          return reject(err);
        }
      }

      read({ prompt, silent: true, replace: '*' }, (err, result) => {
        if (err) {
          return reject(err);
        }

        try {
          resolve(new Secret(result, salt));
          onReadResult(result);
        } catch (err) {
          reject(err);
        }
      });
    });
  }

  /**
   * @constructor
   * @param {buffer|string} passphrase
   */
  constructor(passphrase, salt) {
    passphrase = passphrase.toString();

    assert.ok(passphrase, 'No passphrase supplied');
    assert(passphrase.length >= 8, 'Passphrase is less than 8 characters');
    assert(passphrase !== passphrase.toLowerCase(),
      'Passphrase must contain upper and lower case characters');
    assert(passphrase !== passphrase.toUpperCase(),
      'Passphrase must contain upper and lower case characters');

    this.encrypt = encrypt.bind(null, key(passphrase, salt));
    this.decrypt = decrypt.bind(null, key(passphrase, salt));
  }

}

module.exports = Secret;
