'use strict';

const ini = require('ini');
const { existsSync, writeFileSync } = require('fs');
const mkdirp = require('mkdirp');
const { homedir } = require('os');
const { join } = require('path');

const DEFAULT_DATADIR = join(homedir(), '.config/orcd');

module.exports = function(datadir) {
  /* eslint max-len: ["error", { "code": 100 }]  */

  datadir = datadir || DEFAULT_DATADIR;

  const options = {

    // Process PID
    DaemonPidFilePath: join(datadir, 'orc.pid'),

    // Identity/Cryptography
    PrivateKeyPath: join(datadir, 'orc_ecdsa'),
    IdentityNoncePath: join(datadir, 'eq_nonce'),
    IdentityProofPath: join(datadir, 'eq_proof'),
    CryptParamsFilePath: join(datadir, 'crypt_params'),
    TwoFactorAuthSecretPath: join(datadir, 'otp_secret'),
    PassphraseFile: '',
    HashCashDifficulty: '4',

    // Databases
    NetworkStorageDatabasePath: join(datadir, 'network.crypt'),
    BridgeStorageDatabasePath: join(datadir, 'bridge.crypt'),
    PeerCacheStorageDatabasePath: join(datadir, 'peercache.json'),

    // Node Options
    NodeVirtualPort: '80',
    NodeListenPort: '9088',
    NodeOnionServiceDataDirectory: join(datadir, 'node.tor'),

    // Network Bootstrapping
    NetworkBootstrapNodes: [
      'http://z2ybz7kjxjtfiwcervfh376swy4je3ye4yne2atoi727634qzjonk7id.onion:80',
      'http://kw574zcpisqn2wwiuatqqlknvaosxgh7l73rs2mpabyd3pjf4rm26eid.onion:80',
      'http://cjs4ir3elrpsopvy7piaugjat2zh47tq5x5wt464dc3gncy7ie3ahryd.onion:80',
      'http://fsf52kgowehwwvwdawjx46f76jqob3gmnzyzqajvfzugq4svey5m4oqd.onion:80',
      'http://rxbja7zik7sz3kly5qaud6sgru7utllgme3ofduocl3z6fannja32byd.onion:80',
      'http://x634o3lsbeko7alnwiztkhe75xfgvwccy465gt37ub7il7nep4xczgad.onion:80',
      'http://kzvstvhhtydthakxvfp7ln67xirudxbyejm7n5n773ul57vtvbgjgwqd.onion:80',
      'http://4qjxwjdfpfi3kkpg2qw2v6dhjy77fbszg2udq6wqeqcqllekdiufjyid.onion:80',
      'http://rsfph372b5mljq5tyw7rttjgxmt3gkicpw7773qrhmw32nxaso6hecad.onion:80',
      'http://gdimr6cy2uejr2whygyrki3byyzkytbldcuekv6itwexdu4thppulcyd.onion:80',
      'http://sx4cisml4phxqtopdzt75yv66f2gp6i27hwloamlyraowjbosee2qzad.onion:80',
    ],
    TestNetworkEnabled: '0',

    // Debugging/Developer
    VerboseLoggingEnabled: '1',
    LogFilePath: join(datadir, 'orc.log'),
    LogFileMaxBackCopies: '3',

    // Tor
    TorPassthroughLoggingEnabled: '0',
    TorLoggingVerbosity: 'notice',
    TorSocksProxyVersion: '4',
    TorUseSystemInstall: '0',

    // Local Bridge API/GUI
    BridgeHostname: '127.0.0.1',
    BridgePort: '9089',
    BridgeOnionServiceDataDirectory: join(datadir, 'bridge.tor'),

  };

  if (!existsSync(join(datadir, 'config'))) {
    mkdirp.sync(datadir);
    writeFileSync(join(datadir, 'config'), ini.stringify(options));
  }

  return options;
};
