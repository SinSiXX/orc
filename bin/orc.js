#!/usr/bin/env sh
':' //; exec "$(command -v nodejs || command -v node)" "$0" "$@"

/* eslint strict: 0 */ // eslint doesn't like our hack above

'use strict';

const colors = require('colors/safe');
const async = require('async');
const program = require('commander');
const kadence = require('@deadcanaries/kadence');
const hsv3 = require('@deadcanaries/hsv3');
const ms = require('ms');
const bunyan = require('bunyan');
const RotatingLogStream = require('bunyan-rotating-file-stream');
const fs = require('fs');
const path = require('path');
const orc = require('../index');
const options = require('./config');
const npid = require('npid');
const crypto = require('crypto');
const secp256k1 = require('secp256k1');
const levelup = require('levelup');
const leveldown = require('leveldown');
const encoding = require('encoding-down');
const dbcrypt = require('@adorsys/encrypt-down');
const keytar = require('keytar');
const assert = require('assert');


program.version(`
  orc      ${orc.version.software}
  protocol ${orc.version.protocol}
`);

program.description(`
  Copyright (c) 2019 Dead Canaries, Inc.
  Copyright (c) 2019 Emery Rose Hall
  Licensed under the GNU Affero General Public License Version 3
`);

program.option('--config <file>', 'path to a orc configuration file');
program.option('--datadir <path>', 'path to the default data directory');
program.option('--shutdown', 'sends the shutdown signal to the daemon');
program.option('--testnet', 'runs with reduced identity difficulty');
program.option('--daemon', 'sends orc to the background');
program.option('--reset-passphrase', 'reset passphrase for protecting secret');
program.option('--write-passphrase <path>',
  'write passphrase for protecting secret to file');
program.option('--passphrase <file>',
  'supply the passphrase from a file (non-interactive)');
program.option('--logs-stdout', 'print log messages to stdout instead of file');
program.option('--dump-secret', 'decrypt and print the private extended key');
program.option('--use-system-tor',
  'use the system tor binary instead of the bundled one');
program.option('--disable-keyring',
  'do not try to load decryption secret from system keyring');
program.parse(process.argv);

let argv;

if (program.datadir && !program.config) {
  argv = { config: path.join(program.datadir, 'config') };
}

if (program.testnet) {
  process.env.orcd_TestNetworkEnabled = '1';
}

if (program.useSystemTor) {
  process.env.orcd_TorUseSystemInstall = '1';
}

let config = require('rc')('orcd', options(program.datadir), argv);
let privkey, identity, logger, bridge, nonce, proof, secret;

kadence.constants.T_RESPONSETIMEOUT = ms('30s');

if (parseInt(config.TorUseSystemInstall)) {
  process.env.GRANAX_USE_SYSTEM_TOR = '1';
}

if (parseInt(config.TestNetworkEnabled)) {
  console.warn(`  ${colors.yellow.bold('WARNING:')}`,
    'ORC is running in test mode, difficulties are reduced');
  process.env.orcd_TestNetworkEnabled = config.TestNetworkEnabled;
  kadence.constants.IDENTITY_DIFFICULTY = kadence.constants.TESTNET_DIFFICULTY;
}

if (config.PassphraseFile && !program.passphrase) {
  program.passphrase = config.PassphraseFile;
}

async function resetPassphrase() {
  const { salt } = readCryptParams();
  let prv, oldSecret, newSecret;

  _printLogoAndCopyright();

  if (!fs.existsSync(config.PrivateKeyPath)) {
    prv = kadence.utils.generatePrivateKey();
  } else {
    prv = fs.readFileSync(config.PrivateKeyPath);
  }

  if (prv.length !== 32) {
    oldSecret = await requestUserPassphrase(
      colors.bold('  Enter the passphrase to decrypt your private key:'),
      salt,
      null,
      { readFromKeyring: false, saveToKeyring: false }
    );
    try {
      prv = oldSecret.decrypt(prv);
    } catch (err) {
      console.error(`  ${colors.bold.red('ERROR:')}`,
        'Failed to decrypt, bad passphrase?');
      process.exit(1);
    }
  }

  newSecret = await requestUserPassphrase(
    colors.bold('  Enter a passphrase to encrypt your private key:'),
    salt,
    null,
    { readFromKeyring: false, saveToKeyring: true }
  );

  fs.writeFileSync(config.PrivateKeyPath, newSecret.encrypt(prv));
  console.info('  The private key encryption was reset!');
}

function writeCryptParams() {
  fs.writeFileSync(config.CryptParamsFilePath, crypto.randomBytes(32));
}

function readCryptParams() {
  if (fs.existsSync(config.CryptParamsFilePath)) {
    let params = fs.readFileSync(config.CryptParamsFilePath);
    return {
      salt: params.slice(0, 16),
      iv: params.slice(16)
    }
  } else {
    writeCryptParams();
    return readCryptParams();
  }
}

async function onPassphraseResult(passphrase, saveToKeyring) {
  if (program.writePassphrase) {
    fs.writeFileSync(program.writePassphrase, passphrase);
    console.info(`  Passphrase written to ${program.writePassphrase}.`);
  }

  if (saveToKeyring && !program.disableKeyring) {
    try {
      await keytar.setPassword('ORC', config.PrivateKeyPath, passphrase);
      console.info('  Passphrase stored in system keychain.');
    } catch (err) {
      return console.error(`  ${colors.bold.yellow('WARNING:')}`,
        `Failed to save passphrase to system keyring, ${err.message}.`);
    }
  }
}

const KR_DEFAULTS = {
  readFromKeyring: true,
  saveToKeyring: true
};

async function requestUserPassphrase(text, salt, pfile, opts = KR_DEFAULTS) {
  return new Promise(async function(resolve) {
    if (pfile) {
      if (!fs.existsSync(pfile)) {
        console.error(`  ${colors.bold.red('ERROR:')}`,
          `Failed to read passphrase file, ${pfile} does not exist.`);
        process.exit(1);
      }

      return resolve(new orc.Secret(
        fs.readFileSync(pfile).toString().trim(),
        salt
      ));
    }

    let result, password;

    if (opts.readFromKeyring && !program.disableKeyring) {
      try {
        password = await keytar.getPassword('ORC', config.PrivateKeyPath);
        assert.ok(password, 'not found');
        result = new orc.Secret(password, salt);
        return resolve(result);
      } catch (err) {
        console.error(`  ${colors.bold.red('WARNING:')}`,
          `Failed to get encryption key from system, ${err.message}.`);
      }
    }

    try {
      result = await orc.Secret.request(text, salt, null, function(p) {
        onPassphraseResult(p, opts.saveToKeyring);
      });
    } catch (err) {
      console.error(`  ${colors.bold.red('ERROR:')}`, err.message);
      if (err.message === 'canceled') {
        process.exit(1);
      }
      return resolve(await requestUserPassphrase(text, salt));
    }

    resolve(result);
  });
}

function _getBridgeOnionString() {
  const hnfile = path.join(
    config.BridgeOnionServiceDataDirectory,
    'hidden_service',
    'hostname'
  );

  if (fs.existsSync(hnfile)) {
    return 'http://' + fs.readFileSync(hnfile).toString().trim();
  } else {
    return 'not yet configured';
  }
}

function _printLogoAndCopyright() {
  console.info(colors.dim.magenta(fs.readFileSync(
    path.join(__dirname, '../logo.txt')
  ).toString()));
  console.info(colors.italic('  Copyright (c) 2019 Dead Canaries, Inc.'));
  console.info(colors.italic('  Copyright (c) 2019 Emery Rose Hall'));
  console.info(colors.italic(
    '  Licensed under the GNU Affero General Public License Version 3'
  ));
  console.info('');
}

function _prettyPrintConsole() {
  console.info('');
  console.info(colors.bold('  Fingerprint:'),
    identity.fingerprint.toString('hex'));
  console.info(colors.bold('  Logs:'), config.LogFilePath);
  console.info('');
  console.info(
    colors.bold('  Web UI (local):'),
    `http://${config.BridgeHostname}:${config.BridgePort}`
  );
  console.info(colors.bold('  Web UI (onion):'), _getBridgeOnionString());
  console.info('');
}

async function _init() {
  /* eslint max-statements: [2, 80] */
  /* eslint complexity: [2, 22] */

  if (program.daemon && !program.passphrase) {
    console.error(`  ${colors.bold.red('ERROR:')}`,
      'The --daemon option must be used with --passphrase');
    process.exit(1);
  }

  if (program.resetPassphrase) {
    try {
      return resetPassphrase();
    } catch (err) {
      console.error(`  ${colors.bold.red('ERROR:')}`, err.message);
      process.exit(1);
    }
  }

  if (program.shutdown) {
    try {
      const pidnum = fs.readFileSync(
        config.DaemonPidFilePath
      ).toString().trim()
      process.kill(parseInt(pidnum), 'SIGTERM');
      console.info(`  Sending shutdown signal to daemon (PID ${pidnum})...`);
    } catch (err) {
      console.error(`  ${colors.bold.red('ERROR:')}`,
        'Failed to shutdown daemon, is it running?');
      console.info(
        '  If ORC shutdown uncleanly before, you may need to delete ' +
        `the PID file (${config.DaemonPidFilePath}).`
      );
      process.exit(1);
    }
    console.info('  ORC is shutting down.')
    process.exit();
  }

  const { salt } = readCryptParams();

  _printLogoAndCopyright();

  // Generate a private extended key if it does not exist
  if (!fs.existsSync(config.PrivateKeyPath)) {
    const prv = kadence.utils.generatePrivateKey();

    secret = await requestUserPassphrase(
      colors.bold('  Enter a passphrase to encrypt your private key:'),
      salt,
      program.passphrase
    );
    privkey = prv;

    fs.writeFileSync(config.PrivateKeyPath, secret.encrypt(prv));
  } else {
    const prv = fs.readFileSync(config.PrivateKeyPath);

    if (prv.length === 32) {
      // Is the private key cleartext? Warn the user to rekey!
      console.warn(`  ${colors.yellow.bold('WARNING:')}`,
        'Your private key is stored in cleartext on disk!');
      console.warn(`  ${colors.yellow.bold('WARNING:')}`,
        'To fix this, run: orc --reset-passphrase.');
      console.warn(`  ${colors.yellow.bold('WARNING:')}`,
        'The web interface will be inaccessible until then for security.');
      privkey = prv;
    } else {
      secret = await requestUserPassphrase(
        colors.bold('  Enter the passphrase to decrypt your private key:'),
        salt,
        program.passphrase
      );

      try {
        privkey = secret.decrypt(prv);
      } catch (err) {
        console.error(`  ${colors.bold.red('ERROR:')}`,
          'Failed to decrypt, bad passphrase?');
        process.exit(1);
      }
    }
  }

  if (fs.existsSync(config.IdentityProofPath)) {
    proof = fs.readFileSync(config.IdentityProofPath);
  }

  if (fs.existsSync(config.IdentityNoncePath)) {
    nonce = parseInt(fs.readFileSync(config.IdentityNoncePath).toString());
  }

  if (program.dumpSecret) {
    console.info(`\n  ${colors.bold('SECRET:')}`,
      privkey.toString('hex'), '\n');
    process.exit(0);
  }

  // Initialize identity
  identity = new kadence.eclipse.EclipseIdentity(
    secp256k1.publicKeyCreate(privkey),
    nonce,
    proof
  );

  // If identity is not solved yet, start trying to solve it
  if (!identity.validate()) {
    console.warn(`  ${colors.bold.yellow('WARNING:')}`,
      'identity proof not yet solved, this can take a while');
    await identity.solve();
    fs.writeFileSync(config.IdentityNoncePath, identity.nonce.toString());
    fs.writeFileSync(config.IdentityProofPath, identity.proof);
  }

  const logStreams = [{
    stream: new RotatingLogStream({
      path: config.LogFilePath,
      totalFiles: parseInt(config.LogFileMaxBackCopies),
      rotateExisting: true,
      gzip: true,
      period: '1d',
      threshold: '10m',
      totalSize: '30m'
    })
  }];

  if (program.logsStdout) {
    logStreams.push({ stream: process.stdout });
  }

  // Initialize logging
  logger = bunyan.createLogger({
    name: identity.fingerprint.toString('hex'),
    streams: logStreams,
    level: parseInt(config.VerboseLoggingEnabled) ? 'debug' : 'info'
  });

  _prettyPrintConsole();

  if (program.daemon) {
    if (fs.existsSync(config.DaemonPidFilePath)) {
      console.error(`  ${colors.bold.red('ERROR:')}`,
        'The PID file already exists, is ORC already running?');
      console.info(
        '  If ORC shutdown uncleanly before, you may need to delete ' +
        `the PID file (${config.DaemonPidFilePath}).`
      );
      process.exit(1);
    }
    console.info('  Starting ORC in the background...');
    require('daemon')({ cwd: process.cwd() });
  }

  if (fs.existsSync(config.DaemonPidFilePath)) {
    let pid = fs.readFileSync(config.DaemonPidFilePath).toString();

    if (!require('is-running')(parseInt(pid))) {
      console.warn(`  ${colors.bold.yellow('WARNING:')}`,
        'found a stale pid file and removing it');
      fs.unlinkSync(config.DaemonPidFilePath);
    }
  }

  try {
    npid.create(config.DaemonPidFilePath).removeOnExit();
  } catch (err) {
    logger.error(err.message);
    logger.error('failed to create pid file, is orc already running?');
    console.error(`  ${colors.bold.red('ERROR:')}`,
      'Failed to create PID file, is orc already running?');
    process.exit(1);
  }

  // Shutdown children cleanly on exit
  process.on('exit', killChildrenAndExit);
  process.on('SIGTERM', killChildrenAndExit);
  process.on('SIGINT', killChildrenAndExit);
  process.on('uncaughtException', exitWithError);
  process.on('unhandledRejection', exitWithError);

  function exitWithError(err) {
    console.error(`  ${colors.bold.red('ERROR:')}`,
      'Unrecoverable error occurred!', err);
    npid.remove(config.DaemonPidFilePath);
    logger.error(err.message);
    logger.debug(err.stack);
    process.exit(1);
  }

  init();
}

function killChildrenAndExit() {
  logger.info('exiting, killing child services');
  npid.remove(config.DaemonPidFilePath);
  process.removeListener('exit', killChildrenAndExit);
  process.exit(0);
}

async function init() {
  // Initialize public contact data
  const contact = {
    hostname: '127.0.0.1', // NB: Placeholder (tor plugin overrides this)
    protocol: 'http:',
    port: parseInt(config.NodeVirtualPort)
  };

  // Create a JSON Web Key for encrypting database
  const jwk = {
    kty: 'oct',
    alg: 'A256GCM',
    use: 'enc',
    k: `jwk${kadence.utils.hash160(privkey).toString('hex')}`
  };

  // Initialize network storage engine
  const storage = levelup(encoding(
    dbcrypt(leveldown(config.NetworkStorageDatabasePath), { jwk })
  ));

  // Initialize protocol implementation
  const node = new orc.Node({
    logger,
    contact,
    identity,
    privateKey: privkey,
    storage,
    hashcashDifficulty: parseInt(config.HashCashDifficulty),
    peerCachePath: config.PeerCacheStorageDatabasePath
  });

  // Handle any fatal errors
  node.on('error', (err) => {
    logger.error(err.message.toLowerCase());
  });

  // Establish onion hidden service
  node.plugin(kadence.onion({
    dataDirectory: config.NodeOnionServiceDataDirectory,
    virtualPort: config.NodeVirtualPort,
    localMapping: `127.0.0.1:${config.NodeListenPort}`,
    torrcEntries: {
      CircuitBuildTimeout: 10,
      KeepalivePeriod: 60,
      NewCircuitPeriod: 60,
      NumEntryGuards: 8,
      Log: `${config.TorLoggingVerbosity} stdout`
    },
    passthroughLoggingEnabled: !!parseInt(config.TorPassthroughLoggingEnabled),
    // NB: Defaulted to 4 because https://trac.torproject.org/projects/tor/ticket/29050
    socksVersion: parseInt(config.TorSocksProxyVersion)
  }));

  let bridgeOpts = {
    privateKeyPath: config.PrivateKeyPath,
    secret,
    cryptParams: readCryptParams(),
    logFilePath: config.LogFilePath,
    otpSecretFilePath: config.TwoFactorAuthSecretPath,
    storage: levelup(encoding(
      dbcrypt(leveldown(config.BridgeStorageDatabasePath), { jwk })
    )),
    onionHostname: _getBridgeOnionString,
    passphraseFile: program.passphraseFile,
    syncPassphraseToKeychain: true
  };

  bridge = new orc.Bridge(node, bridgeOpts);

  logger.info(`establishing local bridge on port ${config.BridgePort}`);
  bridge.listen(parseInt(config.BridgePort), config.BridgeHostname, () => {
    // Establish bridge as a hidden service
    hsv3([{
      dataDirectory: path.join(
        config.BridgeOnionServiceDataDirectory,
        'hidden_service'
      ),
      virtualPort: 80,
      localMapping: `localhost:${config.BridgePort}`
    }], {
      DataDirectory: config.BridgeOnionServiceDataDirectory
    }).on('error', err => {
      logger.error('failed to create onion for bridge: %s', err.message);
    }).on('ready', () => {
      const hs = fs.readFileSync(path.join(
        config.BridgeOnionServiceDataDirectory,
        'hidden_service',
        'hostname'
      )).toString();

      logger.info('bridge onion available at %s', hs);
    });
  });

  // Use verbose logging if enabled
  if (!!parseInt(config.VerboseLoggingEnabled)) {
    node.plugin(kadence.logger(logger));
  }

  // Cast network nodes to an array
  if (typeof config.NetworkBootstrapNodes === 'string') {
    config.NetworkBootstrapNodes = config.NetworkBootstrapNodes.trim().split();
  }

  async function joinNetwork(callback) {
    let entry = null;
    let peers = config.NetworkBootstrapNodes.concat(
      await node.peers.getBootstrapCandidates()
    );

    if (peers.length === 0) {
      logger.info('no bootstrap seeds provided and no known profiles');
      logger.info('running in seed mode (waiting for connections)');

      return node.router.events.once('add', (identity) => {
        config.NetworkBootstrapNodes = [
          kadence.utils.getContactURL([
            identity,
            node.router.getContactByNodeId(identity)
          ])
        ];
        joinNetwork(callback)
      });
    }

    logger.info(`joining network from ${peers.length} seeds`);
    async.detectSeries(peers, (seed, done) => {
      let contact = kadence.utils.parseContactURL(seed);

      logger.info(`requesting identity information from ${seed}`);
      node.ping(contact, (err) => {
        if (err) {
          return done(null, false);
        }

        entry = [
          ...node.router.getClosestContactsToKey(node.identity)
        ].shift();

        node.join(contact, (err) => {
          done(null, (err ? false : true) && node.router.size > 1);
        });
      });
    }, (err, result) => {
      if (!result) {
        logger.error('failed to join network, will retry in 1 minute');
        callback(new Error('Failed to join network'));
      } else {
        callback(null, entry);
      }
    });
  }

  logger.info('bootstrapping tor and establishing hidden service');
  node.listen(parseInt(config.NodeListenPort), () => {
    logger.info(
      `node listening on local port ${config.NodeListenPort} ` +
      `and exposed at http://${node.contact.hostname}:${node.contact.port}`
    );

    function ensureConnected(callback) {
      node.router.events.removeListener('remove', _checkIfDisconnected);
      async.retry({
        times: Infinity,
        interval: 60000
      }, done => joinNetwork(done), (err) => {
        if (err) {
          logger.error(err.message);
          return callback(err);
        }

        logger.info(`discovered ${node.router.size} peers from seed`);
        node.router.events.on('remove', _checkIfDisconnected);
        // Remove bootstrap seed if fingerprint was unknown
        node.router.removeContactByNodeId(
          '0000000000000000000000000000000000000000'
        );
        callback();
      });
    }

    function _checkIfDisconnected(fingerprint) {
      logger.debug('peer %s dropped from routing table', fingerprint);

      if (node.router.size === 0) {
        ensureConnected(() => null);
      }
    }

    ensureConnected(function(err) {
      if (err) {
        process.exit(1);
      }
    });
  });
}

_init();
