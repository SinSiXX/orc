<p style="font-size:18px" align="center"><strong>The <a href="https://deadcanaries.gitlab.io/orc">Onion Routed Cloud</a> is a 
decentralized, anonymous, storage and publishing platform designed to protect 
investigative journalists and their sources.</strong></p>
<p align="center">
  Join our <a href="https://keybase.io/team/deadcanaries.orc">Keybase team</a> to chat!
</p>
<div align="center">
  <a href="https://www.npmjs.com/package/@deadcanaries/orc">
    <img src="https://img.shields.io/npm/v/@deadcanaries/orc.svg?style=flat-square" alt="NPM Package">
  </a> | 
  <a href="https://hub.docker.com/r/deadcanaries/orc">
    <img src="https://img.shields.io/docker/pulls/deadcanaries/orc.svg?style=flat-square" alt="Docker Hub">
  </a> | 
  <a href="https://packagecloud.io/deadcanaries/orc">
    <img src="https://img.shields.io/badge/deb-packagecloud.io-844fec.svg?style=flat-square" alt="Debian Repository">
  </a> | 
  <a href="https://gitlab.com/deadcanaries/orc/raw/master/LICENSE">
    <img src="https://img.shields.io/badge/license-AGPLv3-blue.svg?style=flat-square" alt="AGPL-3.0 License">
  </a>
</div>

---

> This documentation is aimed primarily at users of *ORC Server*, designed for 
> volunteers to run on dedicated hardware to strengthen the network. It also 
> exposes a complete web interface for remote access via Tor hidden service, 
> which makes it suitable for access from a mobile device.

### Installation

We provide several installation options. ORC server is designed to run on 
Debian based GNU/Linux systems and accessed remotely via Tor Browser or 
compatible ORC clients. ORC desktop runs on macOS and Linux systems.

Follow our {@tutorial install} to get up and running! Thanks for helping us 
protect journalists!

### Usage

On first run, ORC will generate a fresh configuration and setup the data 
directory. The first time running ORC Desktop may take some time to start. 
Your network identity is being "mined". This is to mitigate spam and other 
abuse.

ORC is designed to **just work** "out of the box". There should 
be no need for manual configuration. However, you may modify the created 
configuration (`/etc/orcd/config` or `~/.config/orcd/config`) as desired 
(see the {@tutorial config}) and kill the process. Once you are satisfied 
with your configuration, run ORC again.

Once started, you can setup your node through the Web UI which is accessible 
via both a local port and onion address printed to the console when running 
`orc` and written to `$ORC_CONFIG_DIR/bridge.tor/hidden_service/hostname`. 
You can also use the guide for {@tutorial api} to interact with you node if you 
are a developer or advanced user! 

### License

Copyright (C) 2019 Dead Canaries, Inc.  

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see http://www.gnu.org/licenses/.
